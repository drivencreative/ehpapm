$(document).ready(function () {
	tinymce.init({
		selector: 'textarea',
		height: 500,
		menubar: false,
		plugins: [
			'advlist autolink lists link image charmap print preview anchor textcolor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code help wordcount'
		],
		toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
		content_css: [
			'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
		]
	});
	// $('#project-description,#ticket-description,#task-description,#comment').summernote({
	// 	height: 300,             // set minimum height of editor
	// 	tabsize: 2,
	// 	codemirror: {
	// 		mode: 'text/html',
	// 		htmlMode: true,
	// 		lineNumbers: true,
	// 		theme: 'monokai'
	// 	}
	// });
	// jQuery.validator.setDefaults({
	// 	debug: true,
	// 	success: "valid"
	// });
	// $('.taskForm, .ticketForm').parsley();
});

$(function () {
	$(document).on('click', '.form-control', function () {
		// Project
		$('input.timepicker').timepicker({
			timeFormat: 'HH:mm',
			interval: 15,
			minTime: '0',
			maxTime: '99:00',
			defaultTime: '0',
			startTime: '00:15',
			dynamic: false,
			dropdown: true,
			scrollbar: true
		});
		$('#deadline').datetimepicker({
			format: 'DD/MM/YYYY HH:mm'
		});
		$('.datetimepicker').datetimepicker({
			format: 'DD/MM/YYYY'
		});
		
	});
	// Remove button click
	$(document).on(
		'click',
		'[data-role="dynamic-fields"] > .task [data-role="remove"]',
		function (e) {
			e.preventDefault();
			$(this).closest('.task').remove();
			$('[data-role="dynamic-fields"] .task').each(function (i, task) {
				$(task).find('.form-control').each(function (n, field) {
					field.name = field.name.replace(/\d/g, i);
				});
			});
		}
	);
	// Add button click
	$(document).on(
		'click',
		'[data-role="dynamic-fields"] > .task [data-role="add"]',
		function (e) {
			e.preventDefault();
			var container = $(this).closest('[data-role="dynamic-fields"]');
			new_field_group = container.children().filter('.task:first-child').clone();
			new_field_group.find('input').each(function () {
				$(this).val('');
			});
			container.append(new_field_group);
			$('[data-role="dynamic-fields"] .task').each(function (i, task) {
				$(task).find('.form-control').each(function (n, field) {
					field.name = field.name.replace(/\d/g, i);
				});
			});
		}
	);
	
});
$('#external-events div.external-event').each(function () {
	
	// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
	// it doesn't need to have a start or end
	var eventObject = {
		title: $.trim($(this).text()) // use the element's text as the event title
	};
	
	// store the Event Object in the DOM element so we can get to it later
	$(this).data('eventObject', eventObject);
	
});