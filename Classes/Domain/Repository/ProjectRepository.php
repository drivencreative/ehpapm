<?php

namespace Peaksourcing\Ehpapm\Domain\Repository;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for Projects
 */
class ProjectRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $query;

    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * @var array
     */
    protected $list;


    /**
     * @return $this
     */
    public function timePerProject()
    {

        $query = $this->createQuery();
//        $query->getQuerySettings()->setReturnRawQueryResult(TRUE);
        $sql = 'SELECT project.uid, project.name, sum(ticket.spent_time) as spentTimeSum, DATE_FORMAT(FROM_UNIXTIME(task.deadline), "%d/%m/%Y") as datum
                FROM tx_ehpapm_domain_model_ticket AS ticket
                LEFT JOIN tx_ehpapm_task_ticket_mm AS task_ticket ON (task_ticket.uid_foreign = ticket.uid)
                LEFT JOIN tx_ehpapm_domain_model_task AS task ON (task_ticket.uid_local = task.uid)
                LEFT JOIN tx_ehpapm_project_task_mm AS project_task ON (project_task.uid_foreign = task.uid)                
                LEFT JOIN tx_ehpapm_domain_model_project AS project ON (project_task.uid_local = project.uid)
                WHERE NOT task.deleted 
                AND NOT task.hidden 
                AND NOT ticket.deleted 
                AND NOT ticket.hidden
                GROUP BY DATE_FORMAT(FROM_UNIXTIME(task.deadline), "%d/%m/%Y"), project.uid';
        $query->statement($sql);
        $this->query = $query->execute(TRUE);
        return $this;
    }

    /**
     * @return array
     */
    public function getQueryResult()
    {
        return $this->query;
    }

    /**
     * @return array
     */
    public function getFormatTimePerProject()
    {
        return $this->list;
    }

}
