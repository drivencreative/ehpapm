<?php

namespace Peaksourcing\Ehpapm\Domain\Repository;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for Tasks
 */
class TaskRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * @var array
     */
    protected $query;

    /**
     * @return $this
     */
    public function timePerTask($fromDate, $toDate, $user, $project, $task)
    {

        $query = $this->createQuery();
//        DebuggerUtility::var_dump([$fromDate, $toDate, $user, $project, $task]);
        $fromDateCondition = $fromDate ? 'WHERE ticket.for_date >= ? ' : '';
        $toDateCondition = $toDate ? 'AND ticket.for_date <= ? ' : '';
        $userCondition = $user ? 'AND fe_users.uid = ? ' : '';
        $projectCondition = $project ? 'AND project.uid = ? ' : '';
        $taskCondition = $task ? 'AND task.uid = ? ' : '';
//        $query->getQuerySettings()->setReturnRawQueryResult(TRUE);
        $sql = 'SELECT  
                        fe_users.uid as user_uid, 
                        ticket.uid as ticket_uid, 
                        task.uid as task_uid, 
                        task.name as task, 
                        project.name as project, 
                        sum(ticket.spent_time_hours) as spentTimeSumHours, 
                        sum(ticket.spent_time_minutes) as spentTimeSumMinutes, 
                        DATE_FORMAT(FROM_UNIXTIME(ticket.for_date), "%d/%m/%Y") as date
                FROM tx_ehpapm_domain_model_ticket AS ticket
                INNER JOIN tx_ehpapm_task_ticket_mm AS task_ticket ON (task_ticket.uid_foreign = ticket.uid)
                INNER JOIN tx_ehpapm_domain_model_task AS task ON (task_ticket.uid_local = task.uid)
                INNER JOIN tx_ehpapm_project_task_mm AS project_task ON (project_task.uid_foreign = task.uid)                
                INNER JOIN tx_ehpapm_domain_model_project AS project ON (project_task.uid_local = project.uid)
                INNER JOIN tx_ehpapm_user_task_mm AS user_task ON (user_task.uid_foreign = task.uid)
                INNER JOIN fe_users ON (user_task.uid_local = fe_users.uid)
                '   . $fromDateCondition
                    . $toDateCondition
                    . $userCondition
                    . $projectCondition
                    . $taskCondition . '
                AND NOT task.deleted 
                AND NOT task.hidden 
                AND NOT ticket.deleted 
                AND NOT ticket.hidden
                GROUP BY fe_users.uid, ticket.uid, task.uid, project.uid, DATE_FORMAT(FROM_UNIXTIME(ticket.for_date), "%d/%m/%Y")';
//        echo $sql;die;
        $query->statement($sql, array_filter([$fromDate, $toDate, $user, $project, $task]));
        $this->query = $query->execute(TRUE);
        return $this;
    }

    /**
     * @return array
     */
    public function getQueryResult()
    {
        return $this->query;
    }

    /**
     * find Tasks by Project
     *
     * @param $project
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findByProject($project)
    {
        $query = $this->createQuery();
        $query->matching($query->contains('project', $project));
        return $query->execute()->toArray();
    }

    /**
     * find Tasks by User
     *
     * @param $user
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findByUser($user)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                $query->contains('user', $user),
                $query->logicalNot(
                    $query->equals('ticket.uid', 0)
                )
            )
        );
        return $query->execute()->toArray();
    }
}
