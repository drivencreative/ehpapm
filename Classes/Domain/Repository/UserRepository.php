<?php

namespace Peaksourcing\Ehpapm\Domain\Repository;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/
use Peaksourcing\Ehpapm\Domain\Model\User;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for Users
 */
class UserRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'email' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /** @var  \TYPO3\CMS\Extbase\Persistence\QueryInterface */
    protected $query;

    /**
     * @var array
     */
    protected $and = [];

    /**
     * Initialize
     */
    public function initializeObject()
    {
        $querySettings = $this->objectManager->get(Typo3QuerySettings::class);
        $querySettings->setRespectStoragePage(FALSE);
        $this->setDefaultQuerySettings($querySettings);
    }

    /**
     * @return array|QueryResultInterface
     */
    public function executeQuery()
    {
//        DebuggerUtility::var_dump($this->and);
        $this->query->matching($this->query->logicalAnd($this->and));
        return $this->query->execute();
    }

    /**
     * Find users by commaseparated usergroup list
     *
     * @param string $userGroupList commaseparated list of usergroup uids
     * @param array $settings Flexform and TypoScript Settings
     * @return UserRepository
     */
    public function findByUsergroups($userGroupList, $settings)
    {
        $this->query = $this->createQuery();
        // where
        $this->and = [
            $this->query->greaterThan('uid', 0)
        ];
        if (!empty($userGroupList)) {
            $selectedUsergroups = GeneralUtility::trimExplode(',', $userGroupList, true);
            $logicalOr = [];
            foreach ($selectedUsergroups as $group) {
                $logicalOr[] = $this->query->contains('usergroup', $group);
            }
            $this->and[] = $this->query->logicalOr($logicalOr);
        }

        // sorting
        $sorting = QueryInterface::ORDER_ASCENDING;
        if ($settings['list']['sorting'] === 'desc') {
            $sorting = QueryInterface::ORDER_DESCENDING;
        }
        $field = preg_replace('/[^a-zA-Z0-9_-]/', '', $settings['list']['orderby']);
        $this->query->setOrderings([$field => $sorting]);
        // set limit
        if ((int)$settings['list']['limit'] > 0) {
            $this->query->setLimit((int)$settings['list']['limit']);
        }

//        DebuggerUtility::var_dump($this->and);
        return $this;
    }

    /**
     * @param $userUids
     * @return $this
     */
    public function filterByUserUid($userUids)
    {
        $logicalOr = [];
        foreach ($userUids as $uid) {
            $logicalOr[] = $this->query->equals('uid', $uid);
        }
        array_push($this->and, $this->query->logicalOr($logicalOr));

        return $this;
    }
}
