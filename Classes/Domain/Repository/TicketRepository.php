<?php

namespace Peaksourcing\Ehpapm\Domain\Repository;

/***
 *
 * This file is part of the "Ehpa Task Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for Tickets
 */
class TicketRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * find Tasks by Task
     *
     * @param $task
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findByTask($task)
    {
        $query = $this->createQuery();
        $query->matching($query->contains('task', $task));
        return $query->execute()->toArray();
    }

    /**
     * find Tasks by Task
     *
     * @param $task
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findByUids(array $uids)
    {
        $query = $this->createQuery();
        $query->matching($query->in('uid', $uids));
        return $query->execute()->toArray();
    }

    /**
     * find spend time empty Tickets
     *
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findByEmptyTime()
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                $query->equals("spentTimeHours", 0),
                $query->equals("spentTimeMinutes", 0)
            )
        );
        return $query->execute();
    }


    /**
     * find Tickets by User
     *
     * @param $user
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findByUser($user)
    {

        $query = $this->createQuery();
        $userCondition = $user ? 'AND fe_users.uid = ? ' : '';
        $sql = 'SELECT  
                    fe_users.uid as user_uid,
                    fe_users.username as username,
                    fe_users.email as email,
                    ticket.uid as ticket_uid,
                    ticket.description as ticket_name,
                    ticket.spent_time_hours as spent_time_hours,
                    ticket.spent_time_minutes as spent_time_minutes,
                    task.uid as task_uid,
                    task.name as task_name
                FROM tx_ehpapm_domain_model_ticket AS ticket
                INNER JOIN tx_ehpapm_task_ticket_mm AS task_ticket ON (task_ticket.uid_foreign = ticket.uid)
                INNER JOIN tx_ehpapm_domain_model_task AS task ON (task_ticket.uid_local = task.uid)
                INNER JOIN tx_ehpapm_user_task_mm AS user_task ON (user_task.uid_foreign = task.uid)
                INNER JOIN fe_users ON (user_task.uid_local = fe_users.uid)
                ' . $userCondition . '
                AND NOT task.deleted 
                AND NOT task.hidden 
                AND NOT ticket.deleted 
                AND NOT ticket.hidden
                GROUP BY fe_users.uid, ticket.uid, task.uid;';
        $query->statement($sql, array_filter([$user]));
        $this->query = $query->execute(TRUE);
        return $this;
    }


    /**
     * @return array
     */
    public function getQueryResult()
    {
        return $this->query;
    }
}
