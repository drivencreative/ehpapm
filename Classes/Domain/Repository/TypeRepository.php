<?php
namespace Peaksourcing\Ehpapm\Domain\Repository;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/

/**
 * The repository for Types
 */
class TypeRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * find types from comma separated list of uids
     *
     * @param $uidLis
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     * @internal param $categories
     */
    public function findByUids($uidLis){
        $query = $this->createQuery();
        $uids = explode(",",$uidLis);
        foreach($uids as $uid){
            $constrains[] = $query->equals('uid',$uid);
        }
        return $query
                ->matching(
                    $query->logicalOr($constrains)
                )->execute();
    }
}
