<?php

namespace Peaksourcing\Ehpapm\Domain\Repository;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/
use Peaksourcing\Ehpapm\Domain\Model\Type;
use \Peaksourcing\Ehpapm\Domain\Model\User;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for Absences
 */
class AbsenceRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    protected $constraint;
    /**
     * @var array
     */
    protected $query;

    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * Initialize
     */
    public function initializeObject()
    {
        $this->query = $this->createQuery();
        $querySettings = $this->objectManager->get(Typo3QuerySettings::class);
        $querySettings->setRespectStoragePage(FALSE);
        $this->setDefaultQuerySettings($querySettings);
    }

    /**
     * @return $this
     */
    public function filterAll()
    {
        return $this;
    }

    /**
     * @param User $user
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function filterUser(User $user)
    {
        $this->constraint[] = $this->query->logicalOr(
            $this->query->equals('user', $user),
            $this->query->equals('user', 0)
        );
        return $this;
    }

    /**
     * @param $types
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function filterType($types)
    {
        foreach ($types as $type) {
            $this->constraint[] = $this->query->logicalAnd(
                $this->query->logicalNot(
                    $this->query->equals('type', $type->getUid())
                )
            );
        }
        return $this;
    }

    /**
     * @param $payslip
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     * @internal param User $user
     */
    public function filterPayslip($payslip)
    {
        $this->constraint[] = $this->query->logicalAnd(
            $this->query->equals('payslip', $payslip)
        );
        return $this;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return $this->constraint ? $this->query->matching($this->query->logicalAnd($this->constraint))->execute() : $this->query->execute();
    }
}
