<?php

namespace Peaksourcing\Ehpapm\Domain\Model;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/
use Peaksourcing\Ehpapm\Service\FlexformConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Absence
 */
class Absence extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * startDate
     *
     * @var \DateTime
     */
    protected $startDate = null;

    /**
     * endDate
     *
     * @var \DateTime
     */
    protected $endDate = null;

    /**
     * @var bool
     */
    protected $payslip = 0;

    /**
     * type
     *
     * @var \Peaksourcing\Ehpapm\Domain\Model\Type
     */
    protected $type = null;

    /**
     * user
     *
     * @var \Peaksourcing\Ehpapm\Domain\Model\User
     */
    protected $user = null;

    /**
     * comment
     *
     * @var string
     */
    protected $comment = '';

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->type = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getCalendarTitle()
    {
        $payslipMessage = $this->isPayslip() ? ' - Payslip' : '';
        $type = $this->getType() ? ' - ' . $this->getType()->getName() : '';
        return $this->title . $type . $payslipMessage;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the startDate
     *
     * @return \DateTime $startDate
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Returns the startDate
     *
     * @return string
     */
    public function getStart()
    {
        return $this->startDate ? $this->startDate->format(\DateTime::ISO8601) : '';
    }

    /**
     * Sets the startDate
     *
     * @param \DateTime $startDate
     * @return void
     */
    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * Returns the endDate
     *
     * @return \DateTime $endDate
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Returns the endDate
     *
     * @return string
     */
    public function getEnd()
    {
        return $this->endDate ? $this->endDate->format(\DateTime::ISO8601) : '';
    }

    /**
     * Sets the endDate
     *
     * @param \DateTime $endDate
     * @return void
     */
    public function setEndDate(\DateTime $endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * Adds a Type
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Type $type
     * @return void
     */
    public function addType(\Peaksourcing\Ehpapm\Domain\Model\Type $type)
    {
        $this->type->attach($type);
    }

    /**
     * Removes a Type
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Type $typeToRemove The Type to be removed
     * @return void
     */
    public function removeType(\Peaksourcing\Ehpapm\Domain\Model\Type $typeToRemove)
    {
        $this->type->detach($typeToRemove);
    }

    /**
     * Returns the type
     *
     * @return \Peaksourcing\Ehpapm\Domain\Model\Type $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Type $type
     * @return void
     */
    public function setType(\Peaksourcing\Ehpapm\Domain\Model\Type $type)
    {
        $this->type = $type;
    }

    /**
     * Adds a User
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\User $user
     * @return void
     */
    public function addUser(\Peaksourcing\Ehpapm\Domain\Model\User $user)
    {
        $this->user->attach($user);
    }

    /**
     * Removes a User
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\User $userToRemove The User to be removed
     * @return void
     */
    public function removeUser(\Peaksourcing\Ehpapm\Domain\Model\User $userToRemove)
    {
        $this->user->detach($userToRemove);
    }

    /**
     * Returns the user
     *
     * @return \Peaksourcing\Ehpapm\Domain\Model\User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Returns the user
     *
     * @return string
     */
    public function getColor()
    {
        return $this->payslip ? "#015E10" : (!$this->user ? "#5AACEE" : '#965800');
    }

    /**
     * Returns the user
     *
     * @return string
     */
    public function getTextColor()
    {
        return "#FFF";
    }

    /**
     * Sets the user
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\User $user
     * @return void
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;
    }

    /**
     * Get Edit url
     *
     * @return string
     */
    public function getUrl()
    {
        $flexformConfiguration = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class)->get(FlexformConfiguration::class);
        $employeeAbsenceTypesForbidden = GeneralUtility::trimExplode(',', $flexformConfiguration->getSettings()['employeeAbsenceTypesForbidden'], true);

        /** @var $uriBuilder \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder * */
        $uriBuilder = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class)->get(\TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder::class);
        if (in_array($this->getType()->getUid(), $employeeAbsenceTypesForbidden)) {
            return '';
        }
        return $uriBuilder->setCreateAbsoluteUri(true)->setArguments(['tx_ehpapm_absences' => ['action' => 'edit', 'absence' => $this->getUid()]])->build();
    }

    /**
     * @return bool
     */
    public function isPayslip()
    {
        return $this->payslip;
    }

    /**
     * @param bool $payslip
     */
    public function setPayslip(bool $payslip)
    {
        $this->payslip = $payslip;
    }

    /**
     * Returns the comment
     *
     * @return string $comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Sets the comment
     *
     * @param string $comment
     * @return void
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }
}
