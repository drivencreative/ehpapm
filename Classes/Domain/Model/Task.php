<?php
namespace Peaksourcing\Ehpapm\Domain\Model;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/
use Peaksourcing\Ehpapm\Utility\ArrayTool;
use Peaksourcing\Ehpapm\Utility\Dates;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Task
 */
class Task extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * estimateHours
     *
     * @var int
     */
    protected $estimateHours = 0;

    /**
     * estimateMinutes
     *
     * @var int
     */
    protected $estimateMinutes = 0;

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * estimate
     *
     * @var \DateTime
     */
    protected $estimate = null;

    /**
     * deadline
     *
     * @var \DateTime
     */
    protected $deadline = null;

    /**
     * ticket
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\Ticket>
     * @cascade remove
     */
    protected $ticket = null;

    /**
     * project
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\Project>
     * @cascade remove
     */
    protected $project = null;

    /**
     * user
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\User>
     * @lazy
     * @cascade remove
     */
    protected $user = null;

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the estimate
     *
     * @return \DateTime $estimate
     */
    public function getEstimate()
    {
        return $this->estimate;
    }

    /**
     * Sets the estimate
     *
     * @param \DateTime $estimate
     * @return void
     */
    public function setEstimate(\DateTime $estimate)
    {
        $this->estimate = $estimate;
    }

    /**
     * Returns the deadline
     *
     * @return \DateTime $deadline
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Sets the deadline
     *
     * @param \DateTime $deadline
     * @return void
     */
    public function setDeadline(\DateTime $deadline)
    {
        $this->deadline = $deadline;
    }

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->ticket = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->project = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->user = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Ticket
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Ticket $ticket
     * @return void
     */
    public function addTicket(\Peaksourcing\Ehpapm\Domain\Model\Ticket $ticket)
    {
        $this->ticket->attach($ticket);
    }

    /**
     * Removes a Ticket
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Ticket $ticketToRemove The Ticket to be removed
     * @return void
     */
    public function removeTicket(\Peaksourcing\Ehpapm\Domain\Model\Ticket $ticketToRemove)
    {
        $this->ticket->detach($ticketToRemove);
    }

    /**
     * Returns the ticket
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\Ticket> $ticket
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Sets the ticket
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\Ticket> $ticket
     * @return void
     */
    public function setTicket(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * Adds a Project
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Project $project
     * @return void
     */
    public function addProject(\Peaksourcing\Ehpapm\Domain\Model\Project $project)
    {
        $this->project->attach($project);
    }

    /**
     * Removes a Project
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Project $projectToRemove The Project to be removed
     * @return void
     */
    public function removeProject(\Peaksourcing\Ehpapm\Domain\Model\Project $projectToRemove)
    {
        $this->project->detach($projectToRemove);
    }

    /**
     * Returns the project
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\Project> $project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Sets the project
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\Project> $project
     * @return void
     */
    public function setProject(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $project)
    {
        $this->project = $project;
    }

    /**
     * Adds a User
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\User $user
     * @return void
     */
    public function addUser(\Peaksourcing\Ehpapm\Domain\Model\User $user)
    {
        $this->user->attach($user);
    }

    /**
     * Removes a User
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\User $userToRemove The User to be removed
     * @return void
     */
    public function removeUser(\Peaksourcing\Ehpapm\Domain\Model\User $userToRemove)
    {
        $this->user->detach($userToRemove);
    }

    /**
     * Returns the user
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\User> $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets the user
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\User> $user
     * @return void
     */
    public function setUser(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $user)
    {
        $this->user = $user;
    }

    /**
     * Returns estimateHours
     *
     * @return int
     */
    public function getEstimateHours()
    {
        return $this->estimateHours;
    }

    /**
     * Set the estimateHours
     *
     * @param int $estimateHours
     */
    public function setEstimateHours(int $estimateHours)
    {
        $this->estimateHours = $estimateHours;
    }

    /**
     * Returns estimateMinutes
     *
     * @return int
     */
    public function getEstimateMinutes()
    {
        return $this->estimateMinutes;
    }

    /**
     * Set the estimateMinutes
     *
     * @param int estimateMinutes
     */
    public function setEstimateMinutes(int $estimateMinutes)
    {
        $this->estimateMinutes = $estimateMinutes;
    }

    /**
     * Returns SpentTime
     *
     * @return string
     */
    public function getSpentTime()
    {
        $spentTimeHours = array_sum(array_keys(ArrayTool::groupElements($this->ticket, 'spentTimeHours')));
        $spentTimeMinutes = array_sum(array_keys(ArrayTool::groupElements($this->ticket, 'spentTimeMinutes')));
        $spentTime = Dates::minutesToHours($spentTimeHours, $spentTimeMinutes);
        return implode(':',$spentTime);
    }

    /**
     * Returns Overtime
     *
     * @return boolean
     */
    public function isOvertime()
    {
        $spentTimeHours = array_sum(array_keys(ArrayTool::groupElements($this->ticket, 'spentTimeHours')));
        $spentTimeMinutes = array_sum(array_keys(ArrayTool::groupElements($this->ticket, 'spentTimeMinutes')));
        list($spentTimeHours, $spentTimeMinutes) = Dates::minutesToHours($spentTimeHours, $spentTimeMinutes);
        if($spentTimeHours > $this->estimateHours) return true;
        elseif ($spentTimeMinutes > $this->estimateMinutes && $spentTimeHours == $this->estimateHours) return true;
        return false;
    }

    /**
     * Returns true if task spent time is over limit
     *
     * @return boolean
     */
    public function getSpentHours()
    {
        $spentTimeHours = array_sum(array_keys(ArrayTool::groupElements($this->ticket, 'spentTimeHours')));
        $spentTimeMinutes = array_sum(array_keys(ArrayTool::groupElements($this->ticket, 'spentTimeMinutes')));
        list($spentTimeHours, $spentTimeMinutes) = Dates::minutesToHours($spentTimeHours, $spentTimeMinutes);
        return (int)$spentTimeHours;
    }

    /**
     * Returns true if task spent time is over limit
     *
     * @return boolean
     */
    public function getSpentMinutes()
    {
        $spentTimeHours = array_sum(array_keys(ArrayTool::groupElements($this->ticket, 'spentTimeHours')));
        $spentTimeMinutes = array_sum(array_keys(ArrayTool::groupElements($this->ticket, 'spentTimeMinutes')));
        list($spentTimeHours, $spentTimeMinutes) = Dates::minutesToHours($spentTimeHours, $spentTimeMinutes);
        return (int)$spentTimeMinutes;
    }

    /**
     * Returns leftHours
     *
     * @return int
     */
    public function getLeftHours()
    {
        return str_pad(floor($this->getLeftTime() / 60),2,"0",STR_PAD_LEFT);
    }

    /**
     * Returns leftMinutes
     *
     * @return int
     */
    public function getLeftMinutes()
    {
        return abs(str_pad($this->getLeftTime() % 60,2,"0",STR_PAD_LEFT));
    }


    /**
     * Returns leftTime
     *
     * @return string
     */
    public function getLeftTime()
    {
        $estimateMinutes = $this->getEstimateHours() * 60 + $this->getEstimateMinutes();
        $spentMinutes = $this->getSpentHours() * 60 + $this->getSpentMinutes();
        return $estimateMinutes - $spentMinutes;
    }

    /**
     * Returns the label
     *
     * @return string $label
     */
    public function getLabel()
    {
        return $this->getProject()->toArray()[0]->getName() . ' - ' . $this->name;
    }
}
