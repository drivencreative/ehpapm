<?php
namespace Peaksourcing\Ehpapm\Domain\Model;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/

/**
 * Ticket
 */
class Ticket extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * spentTime
     *
     * @var \DateTime
     */
    protected $spentTime = null;

    /**
     * forDate
     *
     * @var \DateTime
     */
    protected $forDate = null;

    /**
     * spentTimeHours
     *
     * @var int
     */
    protected $spentTimeHours = 0;

    /**
     * spentTimeMinutes
     *
     * @var int
     */
    protected $spentTimeMinutes = 0;

    /**
     * @var bool
     */
    protected $logTime = null;

    /**
     * task
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\Task>
     * @cascade remove
     */
    protected $task = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->task = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Task
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Task $task
     * @return void
     */
    public function addTask(\Peaksourcing\Ehpapm\Domain\Model\Task $task)
    {
        $this->task->attach($task);
    }

    /**
     * Removes a Task
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Task $taskToRemove The Task to be removed
     * @return void
     */
    public function removeTask(\Peaksourcing\Ehpapm\Domain\Model\Task $taskToRemove)
    {
        $this->task->detach($taskToRemove);
    }

    /**
     * Returns the task
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\Task> $task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Sets the task
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\Task> $task
     * @return void
     */
    public function setTask(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $task)
    {
        $this->task = $task;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the spentTime
     *
     * @return \DateTime $spentTime
     */
    public function getSpentTime()
    {
        return $this->spentTime;
    }

    /**
     * Sets the spentTime
     *
     * @param \DateTime $spentTime
     * @return void
     */
    public function setSpentTime(\DateTime $spentTime)
    {
        $this->spentTime = $spentTime;
    }

    /**
     * Returns the forDate
     *
     * @return \DateTime $forDate
     */
    public function getForDate()
    {
        return $this->forDate;
    }

    /**
     * Sets the forDate
     *
     * @param \DateTime $forDate
     * @return void
     */
    public function setForDate(\DateTime $forDate)
    {
        $this->forDate = $forDate;
    }

    /**
     * Returns spentTimeHours
     *
     * @return int
     */
    public function getSpentTimeHours()
    {
        return $this->spentTimeHours;
    }

    /**
     * Set the spentTimeHours
     *
     * @param int $spentTimeHours
     */
    public function setSpentTimeHours(int $spentTimeHours)
    {
        $this->spentTimeHours = $spentTimeHours;
    }

    /**
     * Returns spentTimeMinutes
     *
     * @return int
     */
    public function getSpentTimeMinutes()
    {
        return $this->spentTimeMinutes;
    }

    /**
     * Set the spentTimeMinutes
     *
     * @param int spentTimeMinutes
     */
    public function setSpentTimeMinutes(int $spentTimeMinutes)
    {
        $this->spentTimeMinutes = $spentTimeMinutes;
    }

    /**
     * @return bool
     */
    public function isLogTime()
    {
        return $this->logTime;
    }

    /**
     * @param bool $logTime
     */
    public function setLogTime(bool $logTime)
    {
        $this->logTime = $logTime;
    }
}
