<?php
namespace Peaksourcing\Ehpapm\Domain\Model;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/

/**
 * Project
 */
class Project extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * task
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\Task>
     * @cascade remove
     */
    protected $task = null;

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->task = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Task
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Task $task
     * @return void
     */
    public function addTask(\Peaksourcing\Ehpapm\Domain\Model\Task $task)
    {
        $this->task->attach($task);
    }

    /**
     * Removes a Task
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Task $taskToRemove The Task to be removed
     * @return void
     */
    public function removeTask(\Peaksourcing\Ehpapm\Domain\Model\Task $taskToRemove)
    {
        $this->task->detach($taskToRemove);
    }

    /**
     * Returns the task
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\Task> $task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Sets the task
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Peaksourcing\Ehpapm\Domain\Model\Task> $task
     * @return void
     */
    public function setTask(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $task)
    {
        $this->task = $task;
    }
}
