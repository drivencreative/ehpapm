<?php
namespace Peaksourcing\Ehpapm\Service;


use HDNET\Calendarize\Domain\Model\ConfigurationInterface;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class FlexformConfiguration implements SingletonInterface
{
    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     */
    protected $objectManager = null;

    /**
     * @var array
     */
    protected $settings = [];

    public function getSettings()
    {
        if (!$this->settings) {
            if (!$this->objectManager) {
                $this->objectManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
            }
            $typoscript = $this->objectManager->get(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::class)
                ->getConfiguration(
                    \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS
                );
            $this->settings = $this->flexFormConfiguration($typoscript['plugin']);
        }
        return $this->settings;
    }


    /**
     * @var string|int $id
     **/
    private function flexFormConfiguration($id)
    {
        $data = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('pi_flexform', 'tt_content', 'uid=' . $id);

        return GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Service\FlexFormService::class)->convertFlexFormContentToArray($data['pi_flexform'])['settings'];
    }
}