<?php

namespace Peaksourcing\Ehpapm\Service;

use Peaksourcing\Ehpapm\Utility\ArrayTool;
use Peaksourcing\Ehpapm\Utility\Dates;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class Export
{
    /**
     * typeRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\TypeRepository
     * @inject
     */
    protected $typeRepository = null;

    /**
     * absenceRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\AbsenceRepository
     * @inject
     */
    protected $absenceRepository = null;

    /**
     * userRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\UserRepository
     * @inject
     */
    protected $userRepository = null;

    /**
     * projectRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\ProjectRepository
     * @inject
     */
    protected $projectRepository = null;

    /**
     * The response which will be returned by this action controller
     *
     * @var  \TYPO3\CMS\Extbase\Mvc\Web\Response
     * @inject
     */
    protected $response;

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     * @inject
     */
    protected $objectManager;

    /**
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function toExcel($datesData, $itemsPerDate, $itemsPerTask, $allTasks, $args, $totalHours, $totalMinutes)
    {
        /** @var $excel \PHPExcel * */
        $excel = GeneralUtility::makeInstance(\PHPExcel::class);
        $excel->getProperties()->setTitle('Reports');

        /** @var $sheet \PHPExcel_Worksheet * */
        $sheet = $excel->getActiveSheet();
        $excel->setActiveSheetIndex(0);
        $sheet->setTitle('Data');

        $sheet->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
//        $sheet->getStyle('a1:o1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('cecece');
        $sheet->setAutoFilter($sheet->calculateWorksheetDimension());
//        DebuggerUtility::var_dump($args);
//        die;
        if ($args['export']['user']) {
            $user = $this->userRepository->findByUid($args['export']['user']);
        }

        // Set top row label
        $startOfTable = 8;
        $rowIndex = $startOfTable;


        $allItems = ArrayTool::extractByKey($itemsPerDate, 'items');
        $sheet->setCellValueByColumnAndRow(0, $startOfTable, LocalizationUtility::translate('dates', 'ehpapm'));

        foreach ($itemsPerDate as $row => $date) {

            $sheet->setCellValueByColumnAndRow(0, $rowIndex + 1, $row);
            $column = 0;
            foreach (array_values($allTasks) as $task) {
                $sheet->setCellValueByColumnAndRow($column + 1, $startOfTable, $task->getProject()->toArray()[0]->getName() . ' - ' . $task->getName());
                foreach ($allItems as $formatedTask) {
                    if ($task->getUid() == (int)$formatedTask['task_uid'] && $row == $formatedTask['date']) {
                        $sheet->setCellValueByColumnAndRow($column + 1, $rowIndex + 1, Dates::addLeadingZero($formatedTask['spentTimeSumHours']) . ":" . Dates::addLeadingZero($formatedTask['spentTimeSumMinutes']));
                    }
                }
                $column++;
            }
            $sheet->setCellValueByColumnAndRow($column + 1, $rowIndex + 1, Dates::addLeadingZero($date['items']['sumAllHours']) . ':' . Dates::addLeadingZero($date['items']['sumAllMinutes']));
            $rowIndex++;
        }

        $sheet->setCellValueByColumnAndRow($column + 1, $startOfTable, 'Total per date');
        $sheet->setCellValueByColumnAndRow(0, $rowIndex + 1, 'Total per task');
        foreach (array_values($allTasks) as $itemAllTasksKey => $itemAllTasks) {
            foreach ($itemsPerTask as $itemKey => $item) {
                if ($itemKey == $itemAllTasks->getUid()) {
                    $sheet->setCellValueByColumnAndRow($itemAllTasksKey + 1, $rowIndex + 1, Dates::addLeadingZero($item['items']['sumAllHours']) . ":" . Dates::addLeadingZero($item['items']['sumAllMinutes']));
                }
            }
        }
        // Sum of All
        $sheet->setCellValueByColumnAndRow($column + 1, $rowIndex + 1, Dates::addLeadingZero($totalHours) . ":" . Dates::addLeadingZero($totalMinutes));

        //Set Header
        $sheet->setCellValueByColumnAndRow(3, 2, 'TIMESHEET ' . $args['export']['fromDate'] . ' - ' . $args['export']['toDate']);
        $sheet->setCellValueByColumnAndRow($column + 1, 1, 'Employee');
        $sheet->setCellValueByColumnAndRow($column + 1, 2, 'Date');
        $sheet->setCellValueByColumnAndRow($column + 1, 3, 'Signature');
        if ($user) {
            $sheet->setCellValueByColumnAndRow($column + 2, 1, $user->getUserName() ?: $user->getEmail());
        }

        //Set Logo
        $objDrawing = new \PHPExcel_Worksheet_Drawing();    //create object for Worksheet drawing
        $objDrawing->setOffsetX(5);
        $objDrawing->setOffsetY(5);
        $objDrawing->setPath('typo3conf/ext/ehpapm/Resources/Public/Img/logo.png');
        $objDrawing->setWorksheet($sheet);  //save



        $column = 0;
        foreach (array_values($allTasks) as $col) {
            $sheet->getColumnDimensionByColumn($column)->setWidth(20);
            $sheet->getRowDimension(1)->setRowHeight(40);
            $sheet->getStyle('A1:' . $sheet->getColumnDimensionByColumn($column)->getColumnIndex() . '1')->getAlignment()->setWrapText(true);
            $column++;
        }
        $sheet->getColumnDimensionByColumn($column)->setWidth(20);
        $sheet->getStyle('A8:' . $sheet->getColumnDimensionByColumn($column + 1)->getColumnIndex() . '8')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('b0cae6');
        $sheet->getStyle('A8:' . 'A' . $sheet->getRowDimension($rowIndex + 1)->getRowIndex())->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('b0cae6');
        ob_end_clean();
        header("Content-type:text/html;charset=utf-8");
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="timesheets.xls"');

        \PHPExcel_IOFactory::createWriter($excel, 'Excel5')->setPreCalculateFormulas(true)->save('php://output');
        exit;

//        $this->renderHtml($itemsPerDate, $allTasks, $rowIndex, $sheet, $startOfTable, $allItems);


    }

    /**
     * @param $itemsPerDate
     * @param $allTasks
     * @param $rowIndex
     * @param $sheet
     * @param $startOfTable
     * @param $allItems
     */
    public function renderHtml($itemsPerDate, $allTasks, $rowIndex, $sheet, $startOfTable, $allItems): void
    {
        echo "<table>";
        foreach ($itemsPerDate as $row => $date) {
            $rowIndex++;
            echo "<tr>";
            if ($rowIndex == 9) echo "<td>Dates</td>";
            if ($rowIndex > 9) {
                echo "<td>";
                $sheet->setCellValueByColumnAndRow(0, $rowIndex + 1, $row);
                echo $row;
                echo "</td>";
            }
            $column = 0;
            foreach (array_values($allTasks) as $task) {
//                DebuggerUtility::var_dump($rowIndex);
                if ($rowIndex == 9) {
                    echo "<td>";
                    $sheet->setCellValueByColumnAndRow($column + 1, $startOfTable, $task->getProject()->toArray()[0]->getName() . ' - ' . $task->getName());
                    echo $task->getProject()->toArray()[0]->getName() . ' - ' . $task->getName() . ' - ' . $task->getUid();
                    echo "</td>";
                } else {
//                    DebuggerUtility::var_dump($allItems);
                    foreach ($allItems as $formatedTask) {
//                        if((int)$formatedTask['task_uid']==6 && $formatedTask['date'] == '21/12/2017'){
//                            DebuggerUtility::var_dump($formatedTask); die;
//                        }
                        if ((int)$task->getUid() == (int)$formatedTask['task_uid'] && $row == $formatedTask['date']) {
                            echo "<td>";
                            $sheet->setCellValueByColumnAndRow($column + 1, $rowIndex + 1, $formatedTask['spentTimeSumHours'] . ":" . $formatedTask['spentTimeSumMinutes']);
                            echo $formatedTask['spentTimeSumHours'] . ":" . $formatedTask['spentTimeSumMinutes'];
                            echo "</td>";
                        } elseif ($task->getUid() == $formatedTask['task_uid']) {
                            echo "<td>";
                            $sheet->setCellValueByColumnAndRow($column + 1, $rowIndex + 1, '');
                            echo '';
//                                $row . ' - ' . $formatedTask['task_uid'];
                            echo "</td>";
                        }
                    }

                }
                $column++;
            }
            echo "<td>";
            echo "</td>";
            echo "<td>";
            echo $date['sumAllHours'] . ':' . $date['sumAllMinutes'];
            echo "</td>";
            echo "</tr>";
        }
        echo "</table>";
        die;
    }

}