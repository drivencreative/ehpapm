<?php
namespace Peaksourcing\Ehpapm\Controller;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/
use Peaksourcing\Ehpapm\Utility\ArrayTool;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * ProjectController
 */
class ProjectController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * projectRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\ProjectRepository
     * @inject
     */
    protected $projectRepository = null;

    /**
     * userRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\UserRepository
     * @inject
     */
    protected $userRepository = null;

    /**
     * configuration
     *
     * @var array
     */
    protected $configuration = [];

    /**
     * initialize action create
     *
     * @return void
     */
    public function initializeAction()
    {
        if ($this->configuration = reset($this->arguments)) {
            $this->configuration = $this->configuration->getPropertyMappingConfiguration()
                ->allowAllProperties()
                ->skipUnknownProperties()
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                    TRUE
                )
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                    TRUE
                );
        }
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
//        if($this->userRepository
//            ->findByUsergroups($this->settings['usergroup'], $this->settings) // find all Employees
//            ->filterByUserUid([$GLOBALS['TSFE']->fe_user->user['uid']]) // find current user in Employees list
//            ->executeQuery()
//            ->count())
//        {
////            DebuggerUtility::var_dump('employee');
//            // Logged in user is employee
//        }
//        if($this->userRepository
//            ->findByUsergroups($this->settings['admingroup'], $this->settings) // find all Admin
//            ->filterByUserUid([$GLOBALS['TSFE']->fe_user->user['uid']]) // find current user in Admin list
//            ->executeQuery()
//            ->count())
//        {
////            DebuggerUtility::var_dump('admin');
//            // Logged in user is employee
//        }

//        if ($this->settings['usergroup']) {
//            $employees = $this->userRepository->findByUsergroups($this->settings['usergroup'], $this->settings)->executeQuery();
//        }
//        DebuggerUtility::var_dump($GLOBALS['TSFE']->fe_user->user['uid']);
//        DebuggerUtility::var_dump($this->userRepository->findByUsergroups($this->settings['usergroup'],$this->settings)->filterByUserUid([$GLOBALS['TSFE']->fe_user->user['uid']])->executeQuery()->count());
//        DebuggerUtility::var_dump(ArrayTool::recursivelyExtractKey($employees, 'uid'));
        $projects = $this->projectRepository->findAll();
        $this->view->assign('projects', $projects);
    }

    /**
     * action show
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Project $project
     * @return void
     */
    public function showAction(\Peaksourcing\Ehpapm\Domain\Model\Project $project)
    {
        $this->view->assign('project', $project);
    }

    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {

    }

    /**
     * initialize action create
     *
     * @return void
     */
    public function initializeCreateAction()
    {
//        $this->configuration->forProperty('task.*')
//            ->allowAllProperties()
//            ->skipUnknownProperties()
//            ->setTypeConverterOption(
//                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
//                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
//                TRUE
//            )
//            ->setTypeConverterOption(
//                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
//                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
//                TRUE
//            );
//        $this->timeConfiguration();
//        $this->datetimeConfiguration();
    }

    /**
     * Date Configuration for property mapper
     **/
    protected function timeConfiguration()
    {
        foreach ($this->request->getArguments()['newProject']['task'] as $key => $task) {
            $this->configuration->forProperty("task.$key.estimate")->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT,
                'H:i'
            );
        }
    }

    /**
     * Date Configuration for property mapper
     **/
    protected function datetimeConfiguration()
    {
        foreach ($this->request->getArguments()['newProject']['task'] as $key => $task) {
            $this->configuration->forProperty("task.$key.deadline")->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT,
                'd/m/Y H:i'
            );
        }
    }

    /**
     * action create
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Project $project
     * @return void
     */
    public function createAction(\Peaksourcing\Ehpapm\Domain\Model\Project $project)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->projectRepository->add($project);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Project $project
     * @ignorevalidation $project
     * @return void
     */
    public function editAction(\Peaksourcing\Ehpapm\Domain\Model\Project $project)
    {
        $this->view->assign('project', $project);
    }

    /**
     * action update
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Project $project
     * @return void
     */
    public function updateAction(\Peaksourcing\Ehpapm\Domain\Model\Project $project)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->projectRepository->update($project);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Project $project
     * @return void
     */
    public function deleteAction(\Peaksourcing\Ehpapm\Domain\Model\Project $project)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->projectRepository->remove($project);
        $this->redirect('list');
    }
}
