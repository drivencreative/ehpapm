<?php

namespace Peaksourcing\Ehpapm\Controller;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/

use Peaksourcing\Ehpapm\Utility\ArrayTool;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use Carbon\CarbonInterval;
/**
 * TaskController
 */
class TaskController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * taskRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\TaskRepository
     * @inject
     */
    protected $taskRepository = null;

    /**
     * projectRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\ProjectRepository
     * @inject
     */
    protected $projectRepository = null;

    /**
     * userRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\UserRepository
     * @inject
     */
    protected $userRepository = null;

    /**
     * configuration
     *
     * @var array
     */
    protected $configuration = [];

    /**
     * Persistence Manager
     *
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
     * @inject
     */
    protected $persistenceManager;

    /**
     * initialize action create
     *
     * @return void
     */
    public function initializeAction()
    {
        if ($this->configuration = reset($this->arguments)) {
            $this->configuration = $this->configuration->getPropertyMappingConfiguration()
                ->allowAllProperties()
                ->skipUnknownProperties()
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                    TRUE
                )
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                    TRUE
                );
        }
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $args = $this->request->getArguments();
        $tasks = $this->taskRepository->findAll();
        $users = $this->userRepository->findAll();

        if ($this->settings['usergroup']) {
            $users = $this->userRepository->findByUsergroups($this->settings['usergroup'], $this->settings)->executeQuery();
        }
        if ($args['chooseProject']['project']) {
            $tasks = $this->taskRepository->findByProject($args['chooseProject']['project']);
        }
//        DebuggerUtility::var_dump($tasks);
        $projects = $this->projectRepository->findAll();
        $this->view->assign('projects', $projects);
        $this->view->assign('users', $users);
        $this->view->assign('tasks', $tasks);
        $this->view->assign('project', $args['chooseProject']['project']);
    }

    /**
     * action show
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Task $task
     * @return void
     */
    public function showAction(\Peaksourcing\Ehpapm\Domain\Model\Task $task)
    {
        $this->view->assign('task', $task);
    }

    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {
        $args = $this->request->getArguments();
        $users = $this->userRepository->findAll();
        if ($this->settings['usergroup']) {
            $users = $this->userRepository->findByUsergroups($this->settings['usergroup'], $this->settings)->executeQuery();
        }
//        DebuggerUtility::var_dump($users);
        $projects = $this->projectRepository->findAll();
        $this->view->assign('chooseUser', $args['chooseUser']['user']);
        $this->view->assign('users', $users);
        $this->view->assign('projects', $projects);
    }

    /**
     * action create
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Task $task
     * @return void
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function createAction(\Peaksourcing\Ehpapm\Domain\Model\Task $task)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->taskRepository->add($task);
        $this->redirect('list', null, null, ['chooseProject' => ['project' => $task->getProject()->toArray()[0]->getUid()]]);
    }

    /**
     * action edit
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Task $task
     * @ignorevalidation $task
     * @return void
     */
    public function editAction(\Peaksourcing\Ehpapm\Domain\Model\Task $task)
    {
        $args = $this->request->getArguments();
        $users = $this->userRepository->findAll();
        if ($this->settings['usergroup']) {
            $users = $this->userRepository->findByUsergroups($this->settings['usergroup'], $this->settings)->executeQuery();
        }
        $projects = $this->projectRepository->findAll();
        $this->view->assign('chooseUser', $args['chooseUser']['user']);
        $this->view->assign('users', $users);
        $this->view->assign('selectedUsers', ArrayTool::extractByKey($task->getUser()->toArray(), 'uid'));
        $this->view->assign('projects', $projects);
        $this->view->assign('task', $task);
    }

    /**
     * initialize action initializeUpdate
     *
     * @return void
     */
    public function initializeUpdateAction()
    {
        $this->timeConfiguration();
        $this->deadlineConfiguration();
        $this->userConfiguration();
    }

    /**
     * initialize action create
     *
     * @return void
     */
    public function initializeCreateAction()
    {
        $this->timeConfiguration();
        $this->deadlineConfiguration();
//        $this->userConfiguration();
    }

    /**
     * Date Configuration for property mapper
     **/
    protected function timeConfiguration()
    {
        $this->configuration->forProperty('estimate')->setTypeConverterOption(
            \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::class,
            \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT,
            'H:i'
        );
        $this->configuration->forProperty('estimateHours')
            ->allowAllProperties()
            ->skipUnknownProperties()
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                TRUE
            )
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                TRUE
            );
        $this->configuration->forProperty('estimateMinutes')
            ->allowAllProperties()
            ->skipUnknownProperties()
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                TRUE
            )
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                TRUE
            );
    }

    /**
     * Date Configuration for property mapper
     **/
    protected function deadlineConfiguration()
    {
        $this->configuration->forProperty('deadline')->setTypeConverterOption(
            \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::class,
            \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT,
            'd/m/Y H:i'
        );
    }

    /**
     * User Configuration for property mapper
     **/
    private function userConfiguration()
    {
//        DebuggerUtility::var_dump($this->request->getArguments());
        foreach ($this->request->getArguments()['task']['user'] as $key => $task) {
            $this->configuration->forProperty("user.*")
                ->allowAllProperties()
                ->skipUnknownProperties()
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                    TRUE
                )
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                    TRUE
                );
        }
    }

    /**
     * action update
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Task $task
     * @return void
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function updateAction(\Peaksourcing\Ehpapm\Domain\Model\Task $task)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);

        $this->taskRepository->update($task);
        $this->persistenceManager->persistAll();
        $this->redirect('list', null, null, ['chooseProject' => ['project' => $task->getProject()->toArray()[0]->getUid()]]);
    }

    /**
     * action delete
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Task $task
     * @return void
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function deleteAction(\Peaksourcing\Ehpapm\Domain\Model\Task $task)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->taskRepository->remove($task);
        $this->redirect('list', null, null, ['chooseProject' => ['project' => $task->getProject()->toArray()[0]->getUid()]]);
    }


}
