<?php
namespace Peaksourcing\Ehpapm\Controller;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/

/**
 * TypeController
 */
class TypeController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * typeRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\TypeRepository
     * @inject
     */
    protected $typeRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $types = $this->typeRepository->findAll();
        $this->view->assign('types', $types);
    }

    /**
     * action show
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Type $type
     * @return void
     */
    public function showAction(\Peaksourcing\Ehpapm\Domain\Model\Type $type)
    {
        $this->view->assign('type', $type);
    }

    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {

    }

    /**
     * action create
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Type $newType
     * @return void
     */
    public function createAction(\Peaksourcing\Ehpapm\Domain\Model\Type $newType)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->typeRepository->add($newType);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Type $type
     * @ignorevalidation $type
     * @return void
     */
    public function editAction(\Peaksourcing\Ehpapm\Domain\Model\Type $type)
    {
        $this->view->assign('type', $type);
    }

    /**
     * action update
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Type $type
     * @return void
     */
    public function updateAction(\Peaksourcing\Ehpapm\Domain\Model\Type $type)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->typeRepository->update($type);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Type $type
     * @return void
     */
    public function deleteAction(\Peaksourcing\Ehpapm\Domain\Model\Type $type)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->typeRepository->remove($type);
        $this->redirect('list');
    }
}
