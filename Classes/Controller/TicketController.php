<?php

namespace Peaksourcing\Ehpapm\Controller;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/

use Peaksourcing\Ehpapm\Utility\ArrayTool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\ArrayUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Frontend\Exception;

/**
 * TicketController
 */
class TicketController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * taskRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\TaskRepository
     * @inject
     */
    protected $taskRepository = null;

    /**
     * ticketRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\TicketRepository
     * @inject
     */
    protected $ticketRepository = null;

    /**
     * userRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\UserRepository
     * @inject
     */
    protected $userRepository = null;

    /**
     * configuration
     *
     * @var array
     */
    protected $configuration = [];

    /**
     * @var string
     */
    protected $returnAction;

    /**
     * initialize action create
     *
     * @return void
     */
    public function initializeAction()
    {
        if ($this->configuration = reset($this->arguments)) {
            $this->configuration = $this->configuration->getPropertyMappingConfiguration()
                ->allowAllProperties()
                ->skipUnknownProperties()
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                    TRUE
                )
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                    TRUE
                );
        }
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {

        $isLoggedUserAdmin = $this->isAdmin();

        $isLoggedUserEmployee = $this->isEmployee($isLoggedUserAdmin);

        $args = $this->request->getArguments();
        $users = $this->userRepository->findAll();
        $tasks = $this->taskRepository->findAll();
        $tickets = $this->ticketRepository->findAll();

        if ($isLoggedUserEmployee) {
            $tasks = $this->taskRepository->findByUser($GLOBALS['TSFE']->fe_user->user['uid']);
            $tickets = $this->ticketRepository->findByTask($tasks[0]);
            $users = [$this->userRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid'])];
        }

        if ($args['chooseTask']['task']) {
            $tickets = $this->ticketRepository->findByTask($args['chooseTask']['task']);
        }

//        DebuggerUtility::var_dump($tickets);
        $this->view->assign('chooseUser', $args['chooseUser']['user']);
        $this->view->assign('users', $users);
        $this->view->assign('tasks', $tasks);
        $this->view->assign('task', $args['chooseTask']['task']);
        $this->view->assign('tickets', $tickets);
    }

    /**
     * action logTime
     *
     * @return void
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function logTimeAction()
    {
        $args = $this->request->getArguments();
        $users = $this->userRepository->findAll();
        $tickets = $this->ticketRepository->findAll();
        $tasks = $this->taskRepository->findAll();

        $isLoggedUserAdmin = $this->isAdmin();
        $isLoggedUserEmployee = $this->isEmployee($isLoggedUserAdmin);

        if ($isLoggedUserEmployee) {
            $tasks = $this->taskRepository->findByUser($GLOBALS['TSFE']->fe_user->user['uid']);
            $tickets = $this->ticketRepository->findByTask($tasks[0]);
            $users = [$this->userRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid'])];
        }

        if ($args['chooseTask']['task']) {
            $tickets = $this->ticketRepository->findByTask($args['chooseTask']['task']);
        } else {
            $ticketsUids = ArrayTool::extractByKey($this->ticketRepository->findByUser($GLOBALS['TSFE']->fe_user->user['uid'])->getQueryResult(), 'ticket_uid');
            if(!empty($ticketsUids)){
                $tickets = $this->ticketRepository->findByUids($ticketsUids);
            }
            else{
                $tickets = [];
                $this->addFlashMessage('Task is not selected');
            }
        }


        $this->view->assign('chooseUser', $args['chooseUser']['user']);
        $this->view->assign('users', $users);
        $this->view->assign('tasks', $tasks);
        $this->view->assign('task', $args['chooseTask']['task']);
        $this->view->assign('tickets', $tickets);
        $this->view->assign('logTime', 1);
    }

    /**
     * action show
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Ticket $ticket
     * @return void
     */
    public function showAction(\Peaksourcing\Ehpapm\Domain\Model\Ticket $ticket)
    {
        $this->view->assign('ticket', $ticket);
    }

    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {
        $tasks = $this->taskRepository->findAll();
        $this->view->assign('tasks', $tasks);
    }

    /**
     * initialize action create
     *
     * @return void
     */
    public function initializeCreateAction()
    {
        $this->timeConfiguration();
        $this->ticketConfiguration('newTicket');
    }


    /**
     * Ticket Configuration for property mapper
     */
    private function ticketConfiguration($type)
    {
        foreach ($this->request->getArguments()[$type]['task'] as $key => $task) {
            $this->configuration->forProperty("task.*")
                ->allowAllProperties()
                ->skipUnknownProperties()
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                    TRUE
                )
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                    TRUE
                );
        }
    }

    /**
     * Date Configuration for property mapper
     **/
    protected function timeConfiguration()
    {
        $this->configuration->forProperty('spentTime')->setTypeConverterOption(
            \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::class,
            \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT,
            'H:i'
        );
        $this->configuration->forProperty('logTime')
            ->allowAllProperties()
            ->skipUnknownProperties()
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                TRUE
            )
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                TRUE
            );
        $this->configuration->forProperty('spentTimeHours')
            ->allowAllProperties()
            ->skipUnknownProperties()
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                TRUE
            )
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                TRUE
            );
        $this->configuration->forProperty('spentTimeMinutes')
            ->allowAllProperties()
            ->skipUnknownProperties()
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                TRUE
            )
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                TRUE
            );
        $this->configuration->forProperty('forDate')->setTypeConverterOption(
            \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::class,
            \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT,
            'd/m/Y'
        );
    }

    /**
     * action create
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Ticket $newTicket
     * @return void
     */
    public function createAction(\Peaksourcing\Ehpapm\Domain\Model\Ticket $newTicket)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->ticketRepository->add($newTicket);
        $this->redirect('list', null, null, ['chooseTask' => ['task' => $newTicket->getTask()->toArray()[0]->getUid()]]);
    }

    /**
     * action edit
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Ticket $ticket
     * @ignorevalidation $ticket
     * @return void
     */
    public function editAction(\Peaksourcing\Ehpapm\Domain\Model\Ticket $ticket)
    {
        $args = $this->request->getArguments();

        if ($GLOBALS['TSFE']->fe_user->user['uid']) {
            $tasks = $this->taskRepository->findByUser($GLOBALS['TSFE']->fe_user->user['uid']);
        }

        $userGroup = GeneralUtility::trimExplode(',', $GLOBALS['TSFE']->fe_user->user['usergroup'], TRUE);
        $userGroups = GeneralUtility::trimExplode(',', $this->settings['usergroup'], TRUE);
        $adminGroups = GeneralUtility::trimExplode(',', $this->settings['admingroup'], TRUE);

        $isAuthEmployee = !empty(array_intersect($userGroups, $userGroup));
        $isAuthAdmin = !empty(array_intersect($adminGroups, $userGroup));
        if ($this->settings['admingroup'] && $isAuthAdmin) {
            $tasks = $this->taskRepository->findAll();
        }

        $this->view->assign('tasks', $tasks);
        $this->view->assign('task', $ticket->getTask()->toArray()[0]->getUid());
        $this->view->assign('ticket', $ticket);
        $this->view->assign('isAuthEmployee', $isAuthEmployee);
        $this->view->assign('isAuthAdmin', $isAuthAdmin);
        $this->view->assign('logTime', $args['referer']);
    }

    /**
     * initialize action initializeUpdate
     *
     * @return void
     */
    public function initializeUpdateAction()
    {
        $this->timeConfiguration();
        $this->ticketConfiguration('ticket');
    }

    /**
     * action update
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Ticket $ticket
     * @return void
     */
    public function updateAction(\Peaksourcing\Ehpapm\Domain\Model\Ticket $ticket)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->ticketRepository->update($ticket);
        $this->redirect(isset($this->request->getArguments()['ticket']['logTime']) ? $this->request->getArguments()['ticket']['logTime'] : 'list', null, null, ['chooseTask' => ['task' => $ticket->getTask()->toArray()[0]->getUid()]]);
    }

    /**
     * action delete
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Ticket $ticket
     * @return void
     */
    public function deleteAction(\Peaksourcing\Ehpapm\Domain\Model\Ticket $ticket)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->ticketRepository->remove($ticket);
        $this->redirect(isset($this->request->getArguments()['ticket']['logTime']) ? $this->request->getArguments()['ticket']['logTime'] : 'list', null, null, ['chooseTask' => ['task' => $ticket->getTask()->toArray()[0]->getUid()]]);
    }

    /**
     * @return int
     */
    public function isAdmin(): int
    {
        $isLoggedUserAdmin = $this->userRepository
            ->findByUsergroups($this->settings['admingroup'], $this->settings)// find all Admin
            ->filterByUserUid([$GLOBALS['TSFE']->fe_user->user['uid']])// find current user in Admin list
            ->executeQuery()
            ->count();
        return $isLoggedUserAdmin;
    }

    /**
     * @param $isLoggedUserAdmin
     * @return bool
     */
    public function isEmployee($isLoggedUserAdmin): bool
    {
        $isLoggedUserEmployee =
            !$isLoggedUserAdmin &&
            $this->userRepository
                ->findByUsergroups($this->settings['usergroup'], $this->settings)// find all Employees
                ->filterByUserUid([$GLOBALS['TSFE']->fe_user->user['uid']])// find current user in Employees list
                ->executeQuery()
                ->count();
        return $isLoggedUserEmployee;
    }
}
