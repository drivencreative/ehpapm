<?php

namespace Peaksourcing\Ehpapm\Controller;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/
use Peaksourcing\Ehpapm\Utility\ArrayTool;
use Peaksourcing\Ehpapm\Utility\Dates;
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * ReportController
 */
class ReportController extends AbstractApiController
{
    /**
     * Configuration for JsonView
     **/
    protected $model = [
        '_exclude' => ['pid'],
    ];

    /**
     * configuration
     *
     * @var array
     */
    protected $configuration = [];

    /**
     * typeRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\TypeRepository
     * @inject
     */
    protected $typeRepository = null;

    /**
     * userRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\UserRepository
     * @inject
     */
    protected $userRepository = null;

    /**
     * projectRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\ProjectRepository
     * @inject
     */
    protected $projectRepository = null;

    /**
     * taskRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\TaskRepository
     * @inject
     */
    protected $taskRepository = null;

    /**
     * @var array
     */
    protected $exportData = [];

    /**
     * export
     *
     * @var \Peaksourcing\Ehpapm\Service\Export
     * @inject
     */
    protected $export = [];

    /**
     * initialize action create
     *
     * @return void
     */
    public function initializeAction()
    {
        if ($this->configuration = reset($this->arguments)) {
            $this->configuration = $this->configuration->getPropertyMappingConfiguration()
                ->allowAllProperties()
                ->skipUnknownProperties()
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                    TRUE
                )
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                    TRUE
                );
        }
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $args = $this->request->getArguments();
        $users = $this->userRepository->findAll();
        if ($this->settings['usergroup']) {
            $users = $this->userRepository->findByUsergroups($this->settings['usergroup'], $this->settings)->executeQuery();
        }
        $allTasks = $this->taskRepository->findAll();
        if ($args['filter']['user']) {
            $selectedUser = $this->userRepository->findByUid($args['filter']['user']);
        }
        $datesData = [];
        if ($args['filter']) {
            list($datesData, $itemsPerDate, $totalHours, $totalMinutes) = $this->packData($args, $selectedUser);
            $itemsPerTask = $this->sumSubPerKey(ArrayTool::groupElements($datesData, 'task_uid'));
        }

        $this->view->assign('projects', $this->projectRepository->findAll());
        $this->view->assign('tasks', $this->taskRepository->findAll());

        $this->view->assign('allTasks', $allTasks);
        $this->view->assign('itemsPerDate', $itemsPerDate);
        $this->view->assign('itemsPerTask', $itemsPerTask);
        $this->view->assign('datesData', $datesData);
        $this->view->assign('sumAllHours', $totalHours);
        $this->view->assign('sumAllMinutes', $totalMinutes);
        $this->view->assign('args', $args);
        $this->view->assign('users', $users);
    }


    /**
     * action export
     *
     * @return void
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function exportAction()
    {
        $args = $this->request->getArguments();
        if ($args['export']['user']) {
            $selectedUser = $this->userRepository->findByUid($args['export']['user']);
        }
        $dates = Dates::getRangeOfDates($args['export']['fromDate'], $args['export']['toDate']);
        $datesData = [];
        if ($args['export']) {
            list($datesData, $itemsPerDate, $totalHours, $totalMinutes) = $this->packData($args, $selectedUser, 'export');
        }
        $allTasks = $this->taskRepository->findAll()->toArray();
        $itemsPerTask = $this->sumSubPerKey(ArrayTool::groupElements($datesData, 'task_uid'));
//        DebuggerUtility::var_dump($itemsPerDate);
        $this->export->toExcel($datesData, $itemsPerDate, $itemsPerTask, $allTasks, $args, $totalHours, $totalMinutes);
        $this->redirect('list');
    }

    /**
     * @param $args
     * @param $selectedUser
     * @return array
     * @throws \Exception
     */
    public function packData($args, $selectedUser, $form = 'filter')
    {
        $ticketsData = $this->taskRepository->timePerTask(
            strtotime(str_replace('/', '-', $args[$form]['fromDate'])),
            strtotime(str_replace('/', '-', $args[$form]['toDate'])),
            $selectedUser ? $selectedUser->getUid() : null,
            $args[$form]['project'],
            $args[$form]['task']
        )->getQueryResult();
        if (empty($ticketsData)) $this->redirect('list');

        $datesData = ArrayTool::singleFlattenArray($this->getTaskPerDate($ticketsData));

        if ($datesData) {
            $itemsPerDate = $this->sumSubPerKey(ArrayTool::extractByKey($datesData, 'date'));
        }
//        $test = array_map('intval', ArrayTool::recursivelyExtractKey($itemsPerDate, 'sumAllHours'));
        $totalHours = array_sum(array_map('intval', ArrayTool::recursivelyExtractKey($itemsPerDate, 'sumAllHours')));
        $totalMinutes = array_sum(array_map('intval', ArrayTool::recursivelyExtractKey($itemsPerDate, 'sumAllMinutes')));
        list($totalHours, $totalMinutes) = Dates::minutesToHours($totalHours, $totalMinutes);
        $this->exportData = $itemsPerDate;
//        DebuggerUtility::var_dump($this->getTaskPerDate($ticketsData));
//        DebuggerUtility::var_dump($itemsPerDate);
        return array ($datesData, $itemsPerDate, $totalHours, $totalMinutes);
    }

    /**
     * @param $datesData
     * @return mixed
     * @internal param $itemsPerDate
     */
    public function sumSubPerKey($datesData)
    {
        //Group by task
        foreach ($datesData as $date => $dataData) {
            $newDateData[$date]['items'] = ArrayTool::groupElements($dataData, 'task_uid');
        }
        $finalDateData = [];
//        DebuggerUtility::var_dump($newDateData);
        foreach ($newDateData as $date => $dateData) {
            foreach ($dateData as $dateNew => $dateDataData) {
//                  $newDateData[$date]['items'][]['spentTimeSumHours'] = $value;

                foreach ($dateDataData as $key => $subDateDataData) {
                    if (count($subDateDataData) > 1) {
                        foreach ($subDateDataData as $subKey => $a) {
//                            DebuggerUtility::var_dump($a, 'before');
                            $spentTimeSumHours = array_sum(array_column($subDateDataData, 'spentTimeSumHours'));
                            $spentTimeSumMinutes = array_sum(array_column($subDateDataData, 'spentTimeSumMinutes'));
//                            $a = reset($a);
//                            DebuggerUtility::var_dump($spentTimeSumHours, 'spentTimeSumHours');
                            $a['spentTimeSumHours'] = $spentTimeSumHours;
                            $a['spentTimeSumMinutes'] = $spentTimeSumMinutes;
//                            DebuggerUtility::var_dump($a, 'after');
                        }
                        $finalDateData[$date]['items'][$a['task_uid']] = $a;
                    } else {
//                        DebuggerUtility::var_dump(array_values($subDateDataData)[0]);
                        $finalDateData[$date]['items'][array_values($subDateDataData)[0]['task_uid']] = array_values($subDateDataData)[0];
                    }

                }

            }
        }

        $itemsPerDate = [];
        foreach ($finalDateData as $date => $dataData) {
            if (is_array($dataData)) {
                // Sum spentTime for all tickets with same date and task

                $itemsPerDate[$date] = $dataData;
                foreach ($dataData as $item) {
                    foreach ($item as $subItem) {
                        $itemsPerDate[$date]['items']['sumAllHours'] += $subItem['spentTimeSumHours'];
                        $itemsPerDate[$date]['items']['sumAllMinutes'] += $subItem['spentTimeSumMinutes'];
                    }
                }
                list($itemsPerDate[$date]['items']['sumAllHours'], $itemsPerDate[$date]['items']['sumAllMinutes']) = Dates::minutesToHours($itemsPerDate[$date]['items']['sumAllHours'], $itemsPerDate[$date]['items']['sumAllMinutes']);
                $itemsPerDate[$date]['items']['sumAllHours'] = Dates::addLeadingZero($itemsPerDate[$date]['items']['sumAllHours']);
                $itemsPerDate[$date]['items']['sumAllMinutes'] = Dates::addLeadingZero($itemsPerDate[$date]['items']['sumAllMinutes']);
            }
        }
//        DebuggerUtility::var_dump($itemsPerDate);
        return $itemsPerDate;
    }


    /**
     * @param $ticketsData
     * @return array
     * @internal param $datesData
     */
    public function getTaskPerDate($ticketsData)
    {

        $datesData = [];
        foreach ($ticketsData as $key => $item) {
            $datesData[$item['date']][$item['ticket_uid']] = $item;
        }
        ksort($datesData, SORT_NUMERIC);
        return $datesData;
    }
}
