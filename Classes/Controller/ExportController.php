<?php

namespace Peaksourcing\Ehpapm\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use Peaksourcing\Ehpapm\Service\Export;
use Peaksourcing\Ehpapm\Utility\ArrayTool;
use Peaksourcing\Ehpapm\Utility\Dates;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * ExportController
 */
class ExportController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{


    /**
     * typeRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\TypeRepository
     * @inject
     */
    protected $typeRepository = null;

    /**
     * absenceRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\AbsenceRepository
     * @inject
     */
    protected $absenceRepository = null;

    /**
     * userRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\UserRepository
     * @inject
     */
    protected $userRepository = null;

    /**
     * projectRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\ProjectRepository
     * @inject
     */
    protected $projectRepository = null;

    /**
     * export
     *
     * @var \Peaksourcing\Ehpapm\Service\Export
     * @inject
     */
    protected $export = null;

    /**
     * action index
     *
     * @return void
     */
    public function indexAction()
    {
//        $this->view->assign('registrations', $this->getRegistrations());
    }

    /**
     * action export
     *
     * @return void
     */
    public function exportAction()
    {
        $this->export->toExcel();
    }

}
