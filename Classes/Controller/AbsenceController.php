<?php

namespace Peaksourcing\Ehpapm\Controller;

/***
 *
 * This file is part of the "Ehpa Project Managment" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>, Peak Sourcing
 *
 ***/
use Peaksourcing\Ehpapm\Domain\Model\User;
use Peaksourcing\Ehpapm\Utility\ArrayTool;
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * AbsenceController
 */
class AbsenceController extends AbstractApiController
{
    /**
     * Configuration for JsonView
     **/
    protected $model = [
        '_exclude' => ['pid'],
    ];

    /**
     * configuration
     *
     * @var array
     */
    protected $configuration = [];

    /**
     * initialize action create
     *
     * @return void
     */
    public function initializeAction()
    {

        if ($this->configuration = reset($this->arguments)) {
            $this->configuration = $this->configuration->getPropertyMappingConfiguration()
                ->allowAllProperties()
                ->skipUnknownProperties()
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                    true
                )
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                    true
                );
        }
    }

    /**
     * typeRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\TypeRepository
     * @inject
     */
    protected $typeRepository = null;

    /**
     * absenceRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\AbsenceRepository
     * @inject
     */
    protected $absenceRepository = null;

    /**
     * userRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\UserRepository
     * @inject
     */
    protected $userRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $args = $this->request->getArguments();
        $selectedUserUid = is_array($args['chooseUser']['user']) ? $args['chooseUser']['user']['__identity'] : $args['chooseUser']['user'];
        if (!empty($this->settings['employeeAbsenceTypesForbidden'])) {
            $employeeAbsenceTypesForbidden = $this->typeRepository->findByUids($this->settings['employeeAbsenceTypesForbidden']);
        }
        //        DebuggerUtility::var_dump($args);
//        die;
        $isLoggedUserEmployee =
            !$this->userRepository
                ->findByUsergroups($this->settings['admingroup'], $this->settings)// find all Admin
                ->filterByUserUid([$GLOBALS['TSFE']->fe_user->user['uid']])// find current user in Admin list
                ->executeQuery()
                ->count() &&
            $this->userRepository
                ->findByUsergroups($this->settings['usergroup'], $this->settings)// find all Employees
                ->filterByUserUid([$GLOBALS['TSFE']->fe_user->user['uid']])// find current user in Employees list
                ->executeQuery()
                ->count();
        $absences = $this->absenceRepository->filterAll();
        if ($selectedUserUid) {
//            DebuggerUtility::var_dump($selectedUserUid);die;
            /** @var User $selectedUser */
            $selectedUser = $this->userRepository->findByUid($selectedUserUid);
            $absences = $absences->filterUser($selectedUser);
        }
        if ($args['chooseUser']['payslip']) {
            $absences = $absences->filterPayslip(1);
        }
        if ($isLoggedUserEmployee) {
            /** @var User $selectedUser */
            $selectedUser = $this->userRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
            $absences = $absences->filterUser($selectedUser)->filterPayslip(0)->filterType($employeeAbsenceTypesForbidden);
//        DebuggerUtility::var_dump($absences->filterUser($selectedUser)->filterPayslip(0)->filterType($employeeAbsenceTypesForbidden));
//        DebuggerUtility::var_dump($absences->filterUser($selectedUser)->filterPayslip(0)->filterType($employeeAbsenceTypesForbidden)->getResult());
//        die;
        }


        list($users, $chooseUser) = $this->setUsers($args);
        $this->view->assign('data', $absences->getResult()->toArray());
        $this->view->assign('chooseUser', $chooseUser);
        $this->view->assign('isLoggedUserEmployee', $isLoggedUserEmployee);
        $this->view->assign('payslip', $args['chooseUser']['payslip']);
        $this->view->assign('users', $users);
    }

    /**
     * action show
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Absence $absence
     * @return void
     */
    public function showAction(\Peaksourcing\Ehpapm\Domain\Model\Absence $absence)
    {
        $this->view->assign('absence', $absence);
    }


    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {
        $args = $this->request->getArguments();
        list($users, $chooseUser) = $this->setUsers($args);
        if (!empty($this->settings['employeeAbsenceTypes'])) {
            $employeeAbsenceTypes = $this->typeRepository->findByUids($this->settings['employeeAbsenceTypes']);
        }
        $this->view->assign('chooseUser', $chooseUser);
        $this->view->assign('employeeAbsenceTypes', $employeeAbsenceTypes);
        $this->view->assign('users', $users);
        $this->view->assign('types', $this->typeRepository->findAll()->toArray());
    }

    /**
     * initialize action create
     *
     * @return void
     */
    public function initializeCreateAction()
    {
        $this->dateConfiguration();
        $this->userConfiguration();
    }

    /**
     * initialize update create
     *
     * @return void
     */
    public function initializeUpdateAction()
    {
        $this->dateConfiguration();
        $this->userConfiguration();
    }

    /**
     * action create
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Absence $absence
     * @return void
     */
    public function createAction(\Peaksourcing\Ehpapm\Domain\Model\Absence $absence)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
//        DebuggerUtility::var_dump($absence);
//        die;
        $this->absenceRepository->add($absence);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Absence $absence
     * @ignorevalidation $absence
     * @return void
     */
    public function editAction(\Peaksourcing\Ehpapm\Domain\Model\Absence $absence)
    {
        $args = $this->request->getArguments();
        list($users, $chooseUser) = $this->setUsers($args);
        if (!empty($this->settings['employeeAbsenceTypes'])) {
            $employeeAbsenceTypes = $this->typeRepository->findByUids($this->settings['employeeAbsenceTypes']);
        }
        $this->view->assign('chooseUser', $chooseUser);
        $this->view->assign('employeeAbsenceTypes', $employeeAbsenceTypes);
        $this->view->assign('users', $users);
        $this->view->assign('types', $this->typeRepository->findAll()->toArray());
        $this->view->assign('absence', $absence);
    }

    /**
     * action update
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Absence $absence
     * @return void
     */
    public function updateAction(\Peaksourcing\Ehpapm\Domain\Model\Absence $absence)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->absenceRepository->update($absence);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param \Peaksourcing\Ehpapm\Domain\Model\Absence $absence
     * @return void
     */
    public function deleteAction(\Peaksourcing\Ehpapm\Domain\Model\Absence $absence)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->absenceRepository->remove($absence);
        $this->redirect('list');
    }

    /**
     * User Configuration for property mapper
     **/
    private function userConfiguration()
    {
        $this->configuration->forProperty("user")
            ->allowAllProperties()
            ->skipUnknownProperties()
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                true
            )
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                true
            );
    }

    /**
     * Date Configuration for property mapper
     **/
    protected function dateConfiguration()
    {
        $this->configuration->forProperty('startDate')->setTypeConverterOption(
            \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::class,
            \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT,
            'd/m/Y'
        );
        $this->configuration->forProperty('endDate')->setTypeConverterOption(
            \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::class,
            \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT,
            'd/m/Y'
        );
    }

    /**
     * @return int
     */
    private function currentUserStatus($group)
    {
        return $this->userRepository
            ->findByUsergroups($this->settings[$group], $this->settings)// find all $group users
            ->filterByUserUid([$GLOBALS['TSFE']->fe_user->user['uid']])// find current user in $group list
            ->executeQuery()
            ->count();
    }

    /**
     * @param $args
     * @return array
     */
    public function setUsers($args)
    {
        $selectedUserUid = is_array($args['chooseUser']['user']) ? $args['chooseUser']['user']['__identity'] : $args['chooseUser']['user'];
        $users = $this->userRepository->findAll();
        if (isset($this->settings['usergroup']) && isset($this->settings['admingroup'])) {
            if ($this->currentUserStatus('usergroup')) {

                $chooseUser = $this->userRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
                // users options
                $users = [$this->userRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid'])];

                if ($this->currentUserStatus('admingroup')) {
                    // keep selected users
                    $chooseUser = $this->userRepository->findByUid($selectedUserUid);
                    $users = $this->userRepository->findByUsergroups($this->settings['usergroup'], $this->settings)->executeQuery();
                }
            }
        }
        return array ($users, $chooseUser);
    }

}
