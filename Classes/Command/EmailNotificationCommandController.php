<?php

namespace Peaksourcing\Ehpapm\Command;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Peak Sourcing  <dradisic@peak-sourcing.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use Peaksourcing\Ehpapm\Domain\Model\User;
use Peaksourcing\Ehpapm\System\Utility;
use Peaksourcing\Ehpapm\Utility\ArrayTool;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class EmailNotificationCommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController
{

    /**
     * UriBuilder
     *
     * @var \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder
     * @inject
     */
    protected $uriBuilder = NULL;

    /**
     * MailHandler
     *
     * @var \Peaksourcing\Ehpapm\Service\MailHandler
     * @inject
     */
    protected $mailHandler = null;

    /**
     * ticketRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\TicketRepository
     * @inject
     */
    protected $ticketRepository = null;

    /**
     * taskRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\TaskRepository
     * @inject
     */
    protected $taskRepository = null;

    /**
     * userRepository
     *
     * @var \Peaksourcing\Ehpapm\Domain\Repository\UserRepository
     * @inject
     */
    protected $userRepository = null;

    /**
     * plugin configuration
     *
     * @var string
     */
    protected $plugin = '';

    /**
     * reminder Command
     */
    public function reminderCommand()
    {
        $typoscript = $this->objectManager->get(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::class)
            ->getConfiguration(
                \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT
            );
        $this->plugin = $typoscript['plugin.']['tx_ehpapm_projects.']['settings.']['scheduler.'];


        $this->initTSFE($this->plugin['page']);
        $settings = $this->flexFormConfiguration($this->plugin['plugin']);

        /** @var \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings $querySettings * */
        $querySettings = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings::class);
        if ($this->plugin['folder']) {
            $querySettings->setStoragePageIds(explode(',', $this->plugin['folder']));
        } else {
            $querySettings->setRespectStoragePage(FALSE);
        }
        $this->ticketRepository->setDefaultQuerySettings($querySettings);

        $missingSpentTime =
            ArrayTool::flattenArray(
                ArrayTool::extractByKey(
                    ArrayTool::flattenArray(
                        ArrayTool::extractByKey(
                            $this->ticketRepository->findByEmptyTime()->getQueryResult()->execute(), 'task')
                    ), 'user')
            );
        $missingSpentTimeUsers = array_unique($missingSpentTime);
        $dataList = $this->missingDataPerUser($missingSpentTimeUsers);


        $subject = $settings['reminder']['subject'];
        $sender = $settings['reminder']['senderEmail'];
        $content = $settings['reminder']['content'];

        if ($sender) {
            foreach ($dataList as $userUid => $tickets) {
                /** @var User $user */
                $user = $this->userRepository->findByUid($userUid);
                $receiver = filter_var($user->getEmail(), FILTER_VALIDATE_EMAIL) ? $user->getEmail() : (filter_var($user->getUsername(), FILTER_VALIDATE_EMAIL) ?: $user->getUsername());
                Utility::sendTemplatedMail(
                    $receiver,
                    $sender,
                    $subject,
                    [
                        'user' => $this->userRepository->findByUid($userUid),
                        'tickets' => $tickets,
                        'content' => $content
                    ],
                    '/Mail/SpentTimeReminder.html',
                    null,
                    PATH_typo3conf . 'ext/ehpapm/Resources/Public/Css/mail.css'
                );
            }
        }
    }

    /**
     * @var string|int $id
     **/
    private function flexFormConfiguration($id)
    {
        $data = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('pi_flexform', 'tt_content', 'uid=' . $id);

        return $this->objectManager->get(\TYPO3\CMS\Extbase\Service\FlexFormService::class)->convertFlexFormContentToArray($data['pi_flexform'])['settings'];
    }

    /**
     * @param $page
     */
    public function initTSFE($page)
    {
        $GLOBALS['TT'] = $this->objectManager->get(\TYPO3\CMS\Core\TimeTracker\NullTimeTracker::class);
        $GLOBALS['TT']->start();

        $GLOBALS['TSFE'] = $this->objectManager->get(\TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController::class, $GLOBALS['TYPO3_CONF_VARS'], $page, '');
        $GLOBALS['TSFE']->sys_page = $this->objectManager->get(\TYPO3\CMS\Frontend\Page\PageRepository::class);
        $GLOBALS['TSFE']->initFEuser();
        $GLOBALS['TSFE']->initTemplate();
        $GLOBALS['TSFE']->connectToDB();
        $GLOBALS['TSFE']->determineId();
        $GLOBALS['TSFE']->getFromCache();
        $GLOBALS['TSFE']->getConfigArray();
        $GLOBALS['TSFE']->settingLanguage();
        $GLOBALS['TSFE']->settingLocale();
    }

    /**
     * @param $missingSpentTimeUsers
     * @return array
     */
    public function missingDataPerUser($missingSpentTimeUsers)
    {
        $results = [];
        foreach ($missingSpentTimeUsers as $user) {
            $results[$user->getUid()] = $this->packTasks($user->getUid()) ? $this->packTasks($user->getUid()) : [];
        }
        return $results;
    }

    /**
     * @param $userUid
     * @return array
     */
    public function packTasks($userUid)
    {
        $results = [];
        foreach ($this->ticketRepository->findByUser($userUid)->getQueryResult() as $key => $item) {
            if (empty($item['spent_time_hours']) && empty($item['spent_time_minutes'])) {
                $results[$key]['ticket'] = $this->ticketRepository->findByUid($item['ticket_uid']);
                $results[$key]['link'] = $link = $this->uriBuilder->setCreateAbsoluteUri(true)->setTargetPageUid($this->plugin['page'])->setArguments(['tx_ehpapm_projects' => ['action' => 'edit', 'controller' => 'Ticket', 'ticket' => $item['ticket_uid']]])->buildFrontendUri();;
            }
        }
        return $results;
    }
}