<?php
namespace Peaksourcing\Ehpapm\ViewHelpers;
use \Peaksourcing\Ehpapm\Utility\Dates;

/**
 * Class AddLeadingZeroViewHelper
 */
class AddLeadingZeroViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * @param string $number
     * @return string
     */
    public function render($number)
    {
        return Dates::addLeadingZero($number);
    }
}