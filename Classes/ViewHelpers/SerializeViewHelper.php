<?php
namespace Peaksourcing\Ehpapm\ViewHelpers;

/**
 * Class SerializeViewHelper
 *
 * @package Peaksourcing\Ehpapm\ViewHelpers
 */
class SerializeViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{

	use \Peaksourcing\Ehpapm\Domain\SerializeTrait;

	/**
	 * @param string $content
	 * @param array $conf
	 * @param string $serialize
	 * @return string
	 */
	public function render($content = NULL, $conf = [], $serialize = 'toJson')
	{
		if ($content === NULL) {
			$content = $this->renderChildren();
		}

		return $this->$serialize($conf, $content);
	}

}