<?php

namespace Peaksourcing\Ehpapm\Utility;


use TYPO3\CMS\Extbase\Persistence\Generic\LazyObjectStorage;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Reflection\ObjectAccess;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class ArrayTool
{

    /**
     * The items contained in the collection.
     *
     * @var array
     */
    protected static $items = [];

    /**
     * @param array $elements
     * @param string $groupBy
     *
     * @return array
     */
    public static function groupElements($elements, $groupBy)
    {
        $groups = [];
        foreach ($elements as $key => $value) {
            if (is_array($value)) {
                $currentGroupIndex = isset($value[$groupBy]) ? $value[$groupBy] : null;
            } elseif (is_object($value)) {
                $currentGroupIndex = \TYPO3\CMS\Extbase\Reflection\ObjectAccess::getPropertyPath($value, $groupBy);
            }
            if (is_object($currentGroupIndex)) {
                if ($currentGroupIndex instanceof \TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy) {
                    $currentGroupIndex = $currentGroupIndex->_loadRealInstance();
                }
                $currentGroupIndex = spl_object_hash($currentGroupIndex);
            }
            $groups[$currentGroupIndex][$key] = $value;
        }
        return $groups;
    }

    /**
     * Extract by key
     *
     * @param \Traversable $iterator
     * @param string $key
     * @return array
     */
    public static function extractByKey($iterator, $key)
    {
        $result = \TYPO3\CMS\Extbase\Reflection\ObjectAccess::getPropertyPath($iterator, $key);
        if ($result) {
            return self::flattenArray($result);
        }
        $content = [];
//        DebuggerUtility::var_dump($iterator);
        foreach ($iterator as $index => $v) {
//        DebuggerUtility::var_dump($v);
//        DebuggerUtility::var_dump($key);
            $result = \TYPO3\CMS\Extbase\Reflection\ObjectAccess::getPropertyPath($v, $key);
            if (null !== $result) {
                if (is_array($result)) {
                    foreach ($result as $item) {
                        if (is_object($item)) {
                            $content[$item->getUid()] = $item;
                        } elseif (is_array($item)) {
                            $content[] = $item;
                        }
                    }
                } elseif (is_int($result)) {
                    $content[] = $result;
                } elseif ($result instanceof ObjectStorage) {
                    $content[] = $result->toArray();
                } elseif ($result instanceof \DateTime) {
                    $content[$result->format('HH:mm')][] = $v;
                } else {
//                    $content[] = self::recursivelyExtractKey($v, $key);
                    $content[$result][] = $v;
                }
            } elseif (true === is_array($v) || true === $v instanceof \Traversable) {
                $content[] = self::recursivelyExtractKey($v, $key);
            }
        }
        return $content;
    }


    /**
     * Flatten the result structure, to iterate it cleanly in fluid
     *
     * @param array $content
     * @param array $flattened
     * @return array
     */
    public static function flattenArray(array $content, $flattened = null)
    {
        foreach ($content as $sub) {
            if (is_array($sub)) {
                $flattened = self::flattenArray($sub, $flattened);
            } else {
                $flattened[] = $sub;
            }
        }

        return $flattened;
    }

    /**
     * Single Flatten the result structure
     *
     * @param array $content
     * @param array $flattened
     * @return array
     */
    public static function singleFlattenArray(array $content, $flattened = null)
    {
        foreach ($content as $sub) {
            if (is_array($sub)) {
                foreach ($sub as $subsub) {
                    $flattened[] = $subsub;
                }
            } else if ($sub instanceof LazyObjectStorage) {
                $flattened = $sub->toArray();
            } else {
                $flattened[] = $sub;

            }
        }

        return $flattened;
    }

    /**
     * Recursively extract the key
     *
     * @param \Traversable $iterator
     * @param string $key
     * @return array
     * @throws \Exception
     */
    public static function recursivelyExtractKey($iterator, $key)
    {
        $content = [];

        foreach ($iterator as $v) {
            // Lets see if we find something directly:
            $result = ObjectAccess::getPropertyPath($v, $key);
            if (null !== $result) {
                $content[] = $result;
            } elseif (true === is_array($v) || true === $v instanceof \Traversable) {
                $content[] = static::recursivelyExtractKey($v, $key);
            }
        }

        $content = static::flattenArray($content);

        return $content;
    }

    /**
     * Return only unique items from the collection array.
     *
     * @param  string|callable|null $key
     * @param  bool $strict
     * @return static
     */
    public static function unique($key = null, $strict = false)
    {
        $callback = self::valueRetriever($key);
        $exists = [];

        return self::reject(function ($item, $key) use ($callback, $strict, &$exists) {
            if (in_array($id = $callback($item, $key), $exists, $strict)) {
                return true;
            }
            $exists[] = $id;
        });
    }

    /**
     * Return only unique items from the collection array using strict comparison.
     *
     * @param  string|callable|null $key
     * @return static
     */
    public static function uniqueStrict($key = null)
    {
        return self::unique($key, true);
    }

    /**
     * Reset the keys on the underlying array.
     *
     * @return static
     */
    public function values()
    {
        return new static(array_values(self::$items));
    }

    /**
     * Create a collection of all elements that do not pass a given truth test.
     *
     * @param  callable|mixed $callback
     * @return static
     */
    public static function reject($callback)
    {
        if (self::useAsCallable($callback)) {
            return self::filter(function ($value, $key) use ($callback) {
                return !$callback($value, $key);
            });
        }
        return self::filter(function ($item) use ($callback) {
            return $item != $callback;
        });
    }

    /**
     * Determine if the given value is callable, but not a string.
     *
     * @param  mixed $value
     * @return bool
     */
    protected static function useAsCallable($value)
    {
        return !is_string($value) && is_callable($value);
    }

    /**
     * Get a value retrieving callback.
     *
     * @param  string $value
     * @return callable
     */
    protected static function valueRetriever($value)
    {

        if (self::useAsCallable($value)) {
            return $value;
        }
        return function ($item) use ($value) {
            return self::data_get($item, $value);
        };
    }

    /**
     * Run a filter over each of the items.
     *
     * @param  callable|null $callback
     * @return static
     */
    public static function filter($callback = null)
    {
        if ($callback) {
            return new static(self::where(self::$items, $callback));
        }
        return new static(array_filter(self::$items));
    }

    /**
     * Filter the array using the given callback.
     *
     * @param  array $array
     * @param  callable $callback
     * @return array
     */
    public static function where($array, callable $callback)
    {
        return array_filter($array, $callback, ARRAY_FILTER_USE_BOTH);
    }

    /**
     * Get an item from an array or object using "dot" notation.
     *
     * @param  mixed $target
     * @param  string $key
     * @param  mixed $default
     * @return mixed
     */
    public static function data_get($target, $key, $default = null)
    {
        if (is_null($key)) return $target;
        foreach (explode('.', $key) as $segment) {
            if (is_array($target)) {
                if (!array_key_exists($segment, $target)) {
                    return $default;
                }
                $target = $target[$segment];
            } elseif ($target instanceof ObjectAccess) {
                if (!isset($target[$segment])) {
                    return $default;
                }
                $target = $target[$segment];
            } elseif (is_object($target)) {
                if (!isset($target->{$segment})) {
                    return $default;
                }
                $target = $target->{$segment};
            } else {
                return $default;
            }
        }
        return $target;
    }

    public static function arrayMesh($array)
    {
        $array = self::flattenArray($array, 'date');
        if (count(array_unique($array)) === 1 && end($array) === 'true') {

            DebuggerUtility::var_dump($array);

        }
//        foreach ($array as $item){
//
//        }
//        return $out;
    }
}