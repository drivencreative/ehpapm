<?php

namespace Peaksourcing\Ehpapm\Utility;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Created by PhpStorm.
 * User: dradisic
 * Date: 12/18/17
 * Time: 10:46 AM
 */
class Dates
{
    public static function getDates($month = 1, $year = 2000)
    {
        $list = array ();
        for ($day = 1; $day <= cal_days_in_month(CAL_GREGORIAN, $month, $year); $day++) {
            $time = mktime(12, 0, 0, $month, $day, $year);
            if (date('m', $time) == $month)
                $list[$day] ['date'] = date('d/m/Y', $time);
            $list[$day]['timestamp'] = $time;
        }
        return $list;
    }

    public static function belongsTo($date, $start, $end)
    {
        $date = new \DateTime($date);
        $start = new \DateTime($start);
        $end = new \DateTime($end);

        if ($date >= $start && $date <= $end) {
            return true;
        }
        return false;
    }


    /**
     * @param $fromDate
     * @param $toDate
     * @return array|\DatePeriod
     */
    public static function getRangeOfDates($fromDate, $toDate)
    {
        $dates = new \DatePeriod(
            new \DateTime(str_replace('/', '-', $fromDate)),
            new \DateInterval('P1D'),
            new \DateTime(str_replace('/', '-', $toDate))
        );
        return $dates;
    }



    /**
     * @param $hours
     * @param $minutes
     * @return mixed
     */
    public static function minutesToHours($hours, $minutes)
    {
        if ($minutes >= 60) {
            $hours += floor($minutes / 60);
            $minutes = ($minutes % 60);
        }
        $hours = self::addLeadingZero($hours);
        $minutes = self::addLeadingZero($minutes);
        return [$hours, $minutes];
    }

    /**
     * @param $number
     * @return string
     */
    public static function addLeadingZero($number)
    {
//        if ($number < 10) $number = sprintf("%02s", $number);
//        DebuggerUtility::var_dump((int)$number);
        return $number<0 ? '-'.sprintf('%02d', abs($number)) : sprintf('%02d', $number);
    }
}