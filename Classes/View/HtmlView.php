<?php
namespace Peaksourcing\Ehpapm\View;

/***************************************************************
 *
 *  Copyright notice
 *
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * A HTML view
 */
class HtmlView extends \TYPO3\CMS\Fluid\View\TemplateView {

    /**
     * Parse string and render the template.
     * If "layoutName" is set in a PostParseFacet callback, it will render the file with the given layout.
     *
     * @param string $html
     * @return string Rendered Template
     * @api
     */
    public function renderHtml($html = null)
    {
        if ($this->controllerContext) {
            $this->baseRenderingContext->setControllerContext($this->controllerContext);
            $this->templateParser->setConfiguration($this->buildParserConfiguration());
        }

        $parsedTemplate = $this->templateParser->parse($html);

        if ($parsedTemplate->hasLayout()) {
            $layoutName = $parsedTemplate->getLayoutName($this->baseRenderingContext);
            $layoutIdentifier = $this->getLayoutIdentifier($layoutName);

            $parsedLayout = $this->templateParser->parse($this->getLayoutSource($layoutName));
            if ($parsedLayout->isCompilable()) {
                $this->templateCompiler->store($layoutIdentifier, $parsedLayout);
            }

            $this->startRendering(self::RENDERING_LAYOUT, $parsedTemplate, $this->baseRenderingContext);
            $output = $parsedLayout->render($this->baseRenderingContext);
            $this->stopRendering();
        } else {
            $this->startRendering(self::RENDERING_TEMPLATE, $parsedTemplate, $this->baseRenderingContext);
            $output = $parsedTemplate->render($this->baseRenderingContext);
            $this->stopRendering();
        }

        return $output;
    }

}
