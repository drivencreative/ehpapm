<?php
namespace Peaksourcing\Ehpapm\System;

/***
 *
 * This file is part of the "Company database" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <info@artif.com>
 *
 ***/
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * Utility
 */
class Utility {

    /**
     * Create HTML from variables and template path, and send email
     *
     * @param string $receiver
     * @param string $sender
     * @param string $subject
     * @param array $variables
     * @param string $template
     * @param string $attachment
     * @param string $css
     * @param bool $debug
     */
    public static function sendTemplatedMail($receiver, $sender, $subject, $variables, $template, $attachment = NULL, $css = NULL, $debug = FALSE) {
        $messageBody = self::getStandaloneView($variables, $template, $css, $debug);

        self::sendEmail($receiver, $sender, $subject, $messageBody, $attachment);
    }

    /**
     * render fluid standalone view
     *
     * @param array $variables objects passed to view
     * @param string $template file name of partial template
     * @param string $css file path of css
     * @param bool $debug
     *
     * @return string
     */
    public static function getStandaloneView($variables, $template, $css = NULL, $debug = FALSE) {
        $templatePathAndFilename = ExtensionManagementUtility::extPath( 'ehpapm') .'Resources/Private/Templates'. $template;

        /** @var StandaloneView $standaloneView */
        $standaloneView = GeneralUtility::makeInstance(\TYPO3\CMS\Fluid\View\StandaloneView::class);
        $standaloneView->getRequest()->setControllerExtensionName('ehpapm');
        $standaloneView->setFormat('html');
        $standaloneView->setTemplateRootPaths([GeneralUtility::getFileAbsFileName('EXT:ehpapm/Resources/Private/Templates')]);
        $standaloneView->setLayoutRootPaths([GeneralUtility::getFileAbsFileName('EXT:ehpapm/Resources/Private/Layouts')]);
        $standaloneView->setPartialRootPaths([GeneralUtility::getFileAbsFileName('EXT:ehpapm/Resources/Private/Partials')]);
        $standaloneView->setTemplatePathAndFilename($templatePathAndFilename);
        $standaloneView->assignMultiple($variables);
        $data = $standaloneView->render();

        $data = self::cssToInline($data, $css);

        if ($debug) { echo $data;die; }

        return $data;
    }

    /**
     * Css to Inline
     *
     * @param string $html
     * @param string $css
     *
     * @return string
     **/
    public static function cssToInline($html, $css = '') {
        if ($css) {
            $cssToInline = new \Peaksourcing\Ehpapm\System\CssToInline($html, file_get_contents($css));
            $html = $cssToInline->emogrify();
        }
        return $html;
    }

    /**
     * Send mail
     *
     * @param string $receiver
     * @param string $sender
     * @param string $subject
     * @param string $messageBody
     * @param mixed $attachment
     *
     * @return bool
     */
    public static function sendEmail($receiver, $sender, $subject, $messageBody, $attachment = NULL) {
        $message = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
        $message->setTo($receiver)->setFrom($sender)->setSubject($subject);
        $message->setBody($messageBody, 'text/html');

        if ($attachment) {
            if (is_array($attachment)) {
                foreach ($attachment as $attach) {
                    $message->attach(\Swift_Attachment::fromPath($attach['url'] ?: $attach));
                }
            } else {
                $message->attach(\Swift_Attachment::fromPath($attachment));
            }
        }

        $message->send();

        return $message->isSent();
    }


}
