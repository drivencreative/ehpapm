plugin.tx_ehpapm_projects {
    view {
        templateRootPaths.0 = EXT:ehpapm/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_ehpapm_projects.view.templateRootPath}
        partialRootPaths.0 = EXT:ehpapm/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_ehpapm_projects.view.partialRootPath}
        layoutRootPaths.0 = EXT:ehpapm/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_ehpapm_projects.view.layoutRootPath}
        widget.TYPO3\CMS\Fluid\ViewHelpers\Widget\PaginateViewHelper.templateRootPath = EXT:ehpapm/Resources/Private/Templates/
        widget.TYPO3\CMS\Fluid\ViewHelpers\Widget\PaginateViewHelper.templateRootPaths {
            10 = EXT:ehpapm/Resources/Private/Templates/
        }
    }

    persistence {
        #storagePid = {$plugin.tx_ehpapm_projects.persistence.storagePid}
        #recursive = 1
    }

    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        #requireCHashArgumentForActionArguments = 1
    }

    settings {
        scheduler {
            page = 230
            plugin = 284
            folder = 226
        }

        employeeAbsenceTypesForbidden
    }
}

plugin.tx_ehpapm_absences < plugin.tx_ehpapm_projects
plugin.tx_ehpapm_absences {
    settings {
        page = 222
        plugin = 279
        folder = 227
    }
}

plugin.tx_ehpapm_reports < plugin.tx_ehpapm_projects
api_tag_absence = PAGE
api_tag_absence {
    config {
        disableAllHeaderCode = 1
        debug = 0
        no_cache = 1
        additionalHeaders {
            10 {
                header = Content-Type: application/json
                replace = 1
            }
        }
    }

    typeNum = 85143514
    10 < tt_content.list.20.ehpapm_absences
}

api_tag_reports < api_tag_absence
api_tag_reports {
    typeNum = 85143515
    10 < tt_content.list.20.ehpapm_reports
}

page {
    includeJS {
        jquery = EXT:ehpapm/Resources/Public/Js/jquery.js
        jquery.forceOnTop = 1
        jqueryui = EXT:ehpapm/Resources/Public/Js/jquery-ui.js
        tinymce = EXT:ehpapm/Resources/Public/tinymce/js/tinymce/tinymce.min.js
        ehpapm = EXT:ehpapm/Resources/Public/Js/ehpapm.js
    }

    includeCSS {
        styles = EXT:provider_ehpa/Resources/Public/Admin/css/styles.css
        ehpapm = EXT:ehpapm/Resources/Public/Css/pm.css
    }
}