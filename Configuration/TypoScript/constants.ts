
plugin.tx_ehpapm_projects {
    view {
        # cat=plugin.tx_ehpapm_projects/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:ehpapm/Resources/Private/Templates/
        # cat=plugin.tx_ehpapm_projects/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:ehpapm/Resources/Private/Partials/
        # cat=plugin.tx_ehpapm_projects/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:ehpapm/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_ehpapm_projects//a; type=string; label=Default storage PID
        #storagePid =
    }
}
