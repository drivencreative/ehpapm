<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_task',
        'label' => 'name',
        'label_alt' => 'name',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'name,description,estimate_hours,estimate_minutes,deadline,ticket,project,user',
        'iconfile' => 'EXT:ehpapm/Resources/Public/Icons/tx_ehpapm_domain_model_task.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, description, estimate_hours, estimate_minutes, deadline, ticket, project, user',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, description, estimate_hours, estimate_minutes, deadline, ticket, project, user, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_ehpapm_domain_model_task',
                'foreign_table_where' => 'AND tx_ehpapm_domain_model_task.pid=###CURRENT_PID### AND tx_ehpapm_domain_model_task.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_task.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'description' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_task.description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            
        ],
        'estimate' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_task.estimate',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 4,
                'eval' => 'time',
                'default' => time()
            ]
        ],
        'estimate_hours' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_task.estimate_hours',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
                'default' => 0
            ]
        ],
        'estimate_minutes' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_task.estimate_minutes',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
                'default' => 0
            ]
        ],
        'deadline' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_task.deadline',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 10,
                'eval' => 'datetime',
                'default' => time()
            ],
        ],
        'ticket' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_task.ticket',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_ehpapm_domain_model_ticket',
                'MM' => 'tx_ehpapm_task_ticket_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 1,
                'multiple' => 0,
            ],

        ],
        'user' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_task.user',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'fe_users',
                'foreign_table_where' => 'AND ORDER BY fe_users.email',
                'MM' => 'tx_ehpapm_user_task_mm',
                'MM_opposite_field' => 'uid_foreign',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'useSortable' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
        ],
        'project' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_project',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_ehpapm_domain_model_project',
                'MM' => 'tx_ehpapm_project_task_mm',
                'MM_opposite_field' => 'uid_foreign',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 1,
                'multiple' => 0,
            ],

        ],
    ],
];
