<?php
defined('TYPO3_MODE') || die();
$tmp_ehpapm_columns = [

    'task' => [
        'exclude' => true,
        'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_task',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectMultipleSideBySide',
            'foreign_table' => 'tx_ehpapm_domain_model_task',
            'MM' => 'tx_ehpapm_user_task_mm',
            'size' => 10,
            'autoSizeMax' => 30,
            'maxitems' => 9999,
            'multiple' => 0,
            'fieldControl' => [
                'editPopup' => [
                    'disabled' => false,
                ],
                'addRecord' => [
                    'disabled' => false,
                ],
                'listModule' => [
                    'disabled' => true,
                ],
            ],
        ],

    ],
    'absence' => [
        'config' => [
            'type' => 'passthrough',
        ]
    ]
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tmp_ehpapm_columns, 1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'fe_users',
    'task',
    '',
    'after:name'
);