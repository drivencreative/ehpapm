<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_absence',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,start_date,end_date,type,user,payslip,comment',
        'iconfile' => 'EXT:ehpapm/Resources/Public/Icons/tx_ehpapm_domain_model_absence.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, start_date, end_date, type, user, payslip, comment',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, start_date, end_date, type, user, payslip, comment, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_ehpapm_domain_model_absence',
                'foreign_table_where' => 'AND tx_ehpapm_domain_model_absence.pid=###CURRENT_PID### AND tx_ehpapm_domain_model_absence.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],
        'start_date' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_absence.start_date',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 10,
                'eval' => 'datetime',
                'default' => time()
            ],
        ],
        'end_date' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_absence.end_date',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 10,
                'eval' => 'datetime',
                'default' => time()
            ],
        ],
        'type' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_absence.type',
            'config' => [
                'type' => 'select',
                'foreign_table' => 'tx_ehpapm_domain_model_type',
            ],

        ],
        'title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_absence.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'user' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_absence.user',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'fe_users',
                'foreign_table_where' => ' AND fe_users.deleted=0 ORDER BY fe_users.email ASC',
//                'MM' => 'tx_ehpapm_absence_user_mm',
                'items' => array(
                    array('All', ''),
                ),
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 1,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],
        ],
        'payslip' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_absence.payslip',
            'config' => [
                'type' => 'check',
            ],
        ],
        'comment' => [
            'exclude' => true,
            'label' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_absence.comment',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],

        ],
    ],
];
