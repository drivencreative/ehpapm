<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Peaksourcing.Ehpapm',
            'Projects',
            'Ehpa Projects'
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Peaksourcing.Ehpapm',
            'Absences',
            'Ehpa Absences'
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Peaksourcing.Ehpapm',
            'Reports',
            'Ehpa Reports'
        );

        $pluginSignature = str_replace('_', '', 'ehpapm') . '_projects';
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:ehpapm/Configuration/FlexForms/flexform_projects.xml');

        $pluginSignature = str_replace('_', '', 'ehpapm') . '_absences';
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:ehpapm/Configuration/FlexForms/flexform_absences.xml');

        $pluginSignature = str_replace('_', '', 'ehpapm') . '_reports';
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:ehpapm/Configuration/FlexForms/flexform_reports.xml');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('ehpapm', 'Configuration/TypoScript', 'Ehpa Project Managment');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ehpapm_domain_model_project', 'EXT:ehpapm/Resources/Private/Language/locallang_csh_tx_ehpapm_domain_model_project.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ehpapm_domain_model_project');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ehpapm_domain_model_task', 'EXT:ehpapm/Resources/Private/Language/locallang_csh_tx_ehpapm_domain_model_task.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ehpapm_domain_model_task');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ehpapm_domain_model_ticket', 'EXT:ehpapm/Resources/Private/Language/locallang_csh_tx_ehpapm_domain_model_ticket.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ehpapm_domain_model_ticket');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ehpapm_domain_model_absence', 'EXT:ehpapm/Resources/Private/Language/locallang_csh_tx_ehpapm_domain_model_absence.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ehpapm_domain_model_absence');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ehpapm_domain_model_type', 'EXT:ehpapm/Resources/Private/Language/locallang_csh_tx_ehpapm_domain_model_type.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ehpapm_domain_model_type');

    }
);
