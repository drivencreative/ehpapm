<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function () {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Peaksourcing.Ehpapm',
            'Projects',
            [
                'Project' => 'list, show, new, create, edit, update, delete',
                'Task' => 'list, show, new, create, edit, update, delete, logTime',
                'Ticket' => 'list, show, new, create, edit, update, delete, logTime',
                'Absence' => 'list, show, new, create, edit, update, delete',
                'Type' => 'list, show, new, create, edit, update, delete',
                'User' => 'list, show, new, create, edit, update, delete'
            ],
            // non-cacheable actions
            [
                'Project' => 'list, show, new, create, edit, update, delete',
                'Task' => 'list, show, new, create, edit, update, delete, logTime',
                'Ticket' => 'list, show, new, create, edit, update, delete, logTime',
                'Absence' => 'list, show, new, create, edit, update, delete',
                'Type' => 'list, show, new, create, edit, update, delete',
                'User' => 'list, show, new, create, edit, update, delete'
            ]
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Peaksourcing.Ehpapm',
            'Absences',
            [
                'Absence' => 'list, show, new, create, edit, update, delete',
                'Type' => 'list, show, new, create, edit, update, delete',
                'User' => 'list, show, new, create, edit, update, delete'
            ],
            // non-cacheable actions
            [
                'Absence' => 'create, update, delete, edit, list',
                'Type' => 'create, update, delete, edit, list',
                'User' => 'create, update, delete, edit, list'
            ]
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Peaksourcing.Ehpapm',
            'Reports',
            [
                'Report' => 'list, show, new, create, edit, update, delete, export',
                'Export' => 'export',
            ],
            // non-cacheable actions
            [
                'Report' => 'list, show, new, create, edit, update, delete, export',
                'Export' => 'export',
            ]
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    projects {
                        iconIdentifier = ehpapm-plugin-projects
                        title = LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_projects.name
                        description = LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_projects.description
                        tt_content_defValues {
                            CType = list
                            list_type = ehpapm_projects
                        }
                    }
                }
                show = *
            }
       }'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    absences {
                        iconIdentifier = ehpapm-plugin-absences
                        title = LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_absence
                        description = LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_absence.description
                        tt_content_defValues {
                            CType = list
                            list_type = ehpapm_absences
                        }
                    }
                }
                show = *
            }
       }'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    reports {
                        iconIdentifier = ehpapm-plugin-reports
                        title = LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_report
                        description = LLL:EXT:ehpapm/Resources/Private/Language/locallang_db.xlf:tx_ehpapm_domain_model_report.description
                        tt_content_defValues {
                            CType = list
                            list_type = ehpapm_reports
                        }
                    }
                }
                show = *
            }
       }'
        );
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

        $iconRegistry->registerIcon(
            'ehpapm-plugin-absences',
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:ehpapm/Resources/Public/Icons/absence.svg']
        );
        $iconRegistry->registerIcon(
            'ehpapm-plugin-projects',
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:ehpapm/Resources/Public/Icons/project.svg']
        );
        $iconRegistry->registerIcon(
            'ehpapm-plugin-reports',
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:ehpapm/Resources/Public/Icons/report.svg']
        );

        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = \Peaksourcing\Ehpapm\Command\EmailNotificationCommandController::class;
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\Peaksourcing\Ehpapm\Command\EmailNotificationCommandController::class] = array (
            'extension' => 'Email Notification',
            'title' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang.xlf:task.name',
            'description' => 'LLL:EXT:ehpapm/Resources/Private/Language/locallang.xlf:task.description',
            'additionalFields' => \TYPO3\CMS\Extbase\Scheduler\FieldProvider::class
        );
    }
);
