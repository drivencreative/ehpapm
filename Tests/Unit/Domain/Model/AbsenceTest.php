<?php
namespace Peaksourcing\Ehpapm\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 */
class AbsenceTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Peaksourcing\Ehpapm\Domain\Model\Absence
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Peaksourcing\Ehpapm\Domain\Model\Absence();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getStartReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getStart()
        );
    }

    /**
     * @test
     */
    public function setStartForDateTimeSetsStart()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setStart($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'start',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEndReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getEnd()
        );
    }

    /**
     * @test
     */
    public function setEndForDateTimeSetsEnd()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setEnd($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'end',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTypeReturnsInitialValueForType()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getType()
        );
    }

    /**
     * @test
     */
    public function setTypeForObjectStorageContainingTypeSetsType()
    {
        $type = new \Peaksourcing\Ehpapm\Domain\Model\Type();
        $objectStorageHoldingExactlyOneType = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneType->attach($type);
        $this->subject->setType($objectStorageHoldingExactlyOneType);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneType,
            'type',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addTypeToObjectStorageHoldingType()
    {
        $type = new \Peaksourcing\Ehpapm\Domain\Model\Type();
        $typeObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $typeObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($type));
        $this->inject($this->subject, 'type', $typeObjectStorageMock);

        $this->subject->addType($type);
    }

    /**
     * @test
     */
    public function removeTypeFromObjectStorageHoldingType()
    {
        $type = new \Peaksourcing\Ehpapm\Domain\Model\Type();
        $typeObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $typeObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($type));
        $this->inject($this->subject, 'type', $typeObjectStorageMock);

        $this->subject->removeType($type);
    }

    /**
     * @test
     */
    public function getUserReturnsInitialValueForUser()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getUser()
        );
    }

    /**
     * @test
     */
    public function setUserForObjectStorageContainingUserSetsUser()
    {
        $user = new \Peaksourcing\Ehpapm\Domain\Model\User();
        $objectStorageHoldingExactlyOneUser = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneUser->attach($user);
        $this->subject->setUser($objectStorageHoldingExactlyOneUser);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneUser,
            'user',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addUserToObjectStorageHoldingUser()
    {
        $user = new \Peaksourcing\Ehpapm\Domain\Model\User();
        $userObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $userObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($user));
        $this->inject($this->subject, 'user', $userObjectStorageMock);

        $this->subject->addUser($user);
    }

    /**
     * @test
     */
    public function removeUserFromObjectStorageHoldingUser()
    {
        $user = new \Peaksourcing\Ehpapm\Domain\Model\User();
        $userObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $userObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($user));
        $this->inject($this->subject, 'user', $userObjectStorageMock);

        $this->subject->removeUser($user);
    }
}
