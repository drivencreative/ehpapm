<?php
namespace Peaksourcing\Ehpapm\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 */
class TaskTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Peaksourcing\Ehpapm\Domain\Model\Task
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Peaksourcing\Ehpapm\Domain\Model\Task();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName()
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'name',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEstimateReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getEstimate()
        );
    }

    /**
     * @test
     */
    public function setEstimateForIntSetsEstimate()
    {
        $this->subject->setEstimate(12);

        self::assertAttributeEquals(
            12,
            'estimate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDeadlineReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDeadline()
        );
    }

    /**
     * @test
     */
    public function setDeadlineForDateTimeSetsDeadline()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDeadline($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'deadline',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTicketReturnsInitialValueForTicket()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getTicket()
        );
    }

    /**
     * @test
     */
    public function setTicketForObjectStorageContainingTicketSetsTicket()
    {
        $ticket = new \Peaksourcing\Ehpapm\Domain\Model\Ticket();
        $objectStorageHoldingExactlyOneTicket = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneTicket->attach($ticket);
        $this->subject->setTicket($objectStorageHoldingExactlyOneTicket);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneTicket,
            'ticket',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addTicketToObjectStorageHoldingTicket()
    {
        $ticket = new \Peaksourcing\Ehpapm\Domain\Model\Ticket();
        $ticketObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $ticketObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($ticket));
        $this->inject($this->subject, 'ticket', $ticketObjectStorageMock);

        $this->subject->addTicket($ticket);
    }

    /**
     * @test
     */
    public function removeTicketFromObjectStorageHoldingTicket()
    {
        $ticket = new \Peaksourcing\Ehpapm\Domain\Model\Ticket();
        $ticketObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $ticketObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($ticket));
        $this->inject($this->subject, 'ticket', $ticketObjectStorageMock);

        $this->subject->removeTicket($ticket);
    }
}
