<?php
namespace Peaksourcing\Ehpapm\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 */
class ProjectTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Peaksourcing\Ehpapm\Domain\Model\Project
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Peaksourcing\Ehpapm\Domain\Model\Project();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName()
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'name',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTaskReturnsInitialValueForTask()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getTask()
        );
    }

    /**
     * @test
     */
    public function setTaskForObjectStorageContainingTaskSetsTask()
    {
        $task = new \Peaksourcing\Ehpapm\Domain\Model\Task();
        $objectStorageHoldingExactlyOneTask = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneTask->attach($task);
        $this->subject->setTask($objectStorageHoldingExactlyOneTask);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneTask,
            'task',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addTaskToObjectStorageHoldingTask()
    {
        $task = new \Peaksourcing\Ehpapm\Domain\Model\Task();
        $taskObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $taskObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($task));
        $this->inject($this->subject, 'task', $taskObjectStorageMock);

        $this->subject->addTask($task);
    }

    /**
     * @test
     */
    public function removeTaskFromObjectStorageHoldingTask()
    {
        $task = new \Peaksourcing\Ehpapm\Domain\Model\Task();
        $taskObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $taskObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($task));
        $this->inject($this->subject, 'task', $taskObjectStorageMock);

        $this->subject->removeTask($task);
    }
}
