<?php
namespace Peaksourcing\Ehpapm\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 */
class UserTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Peaksourcing\Ehpapm\Domain\Model\User
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Peaksourcing\Ehpapm\Domain\Model\User();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getProjectReturnsInitialValueForProject()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getProject()
        );
    }

    /**
     * @test
     */
    public function setProjectForObjectStorageContainingProjectSetsProject()
    {
        $project = new \Peaksourcing\Ehpapm\Domain\Model\Project();
        $objectStorageHoldingExactlyOneProject = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneProject->attach($project);
        $this->subject->setProject($objectStorageHoldingExactlyOneProject);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneProject,
            'project',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addProjectToObjectStorageHoldingProject()
    {
        $project = new \Peaksourcing\Ehpapm\Domain\Model\Project();
        $projectObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $projectObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($project));
        $this->inject($this->subject, 'project', $projectObjectStorageMock);

        $this->subject->addProject($project);
    }

    /**
     * @test
     */
    public function removeProjectFromObjectStorageHoldingProject()
    {
        $project = new \Peaksourcing\Ehpapm\Domain\Model\Project();
        $projectObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $projectObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($project));
        $this->inject($this->subject, 'project', $projectObjectStorageMock);

        $this->subject->removeProject($project);
    }
}
