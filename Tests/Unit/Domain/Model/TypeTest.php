<?php
namespace Peaksourcing\Ehpapm\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 */
class TypeTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Peaksourcing\Ehpapm\Domain\Model\Type
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Peaksourcing\Ehpapm\Domain\Model\Type();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
