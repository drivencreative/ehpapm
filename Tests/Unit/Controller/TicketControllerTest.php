<?php
namespace Peaksourcing\Ehpapm\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 */
class TicketControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Peaksourcing\Ehpapm\Controller\TicketController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Peaksourcing\Ehpapm\Controller\TicketController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllTicketsFromRepositoryAndAssignsThemToView()
    {

        $allTickets = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $ticketRepository = $this->getMockBuilder(\Peaksourcing\Ehpapm\Domain\Repository\TicketRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $ticketRepository->expects(self::once())->method('findAll')->will(self::returnValue($allTickets));
        $this->inject($this->subject, 'ticketRepository', $ticketRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('tickets', $allTickets);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenTicketToView()
    {
        $ticket = new \Peaksourcing\Ehpapm\Domain\Model\Ticket();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('ticket', $ticket);

        $this->subject->showAction($ticket);
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenTicketToTicketRepository()
    {
        $ticket = new \Peaksourcing\Ehpapm\Domain\Model\Ticket();

        $ticketRepository = $this->getMockBuilder(\Peaksourcing\Ehpapm\Domain\Repository\TicketRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $ticketRepository->expects(self::once())->method('add')->with($ticket);
        $this->inject($this->subject, 'ticketRepository', $ticketRepository);

        $this->subject->createAction($ticket);
    }

    /**
     * @test
     */
    public function editActionAssignsTheGivenTicketToView()
    {
        $ticket = new \Peaksourcing\Ehpapm\Domain\Model\Ticket();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('ticket', $ticket);

        $this->subject->editAction($ticket);
    }

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenTicketInTicketRepository()
    {
        $ticket = new \Peaksourcing\Ehpapm\Domain\Model\Ticket();

        $ticketRepository = $this->getMockBuilder(\Peaksourcing\Ehpapm\Domain\Repository\TicketRepository::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $ticketRepository->expects(self::once())->method('update')->with($ticket);
        $this->inject($this->subject, 'ticketRepository', $ticketRepository);

        $this->subject->updateAction($ticket);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenTicketFromTicketRepository()
    {
        $ticket = new \Peaksourcing\Ehpapm\Domain\Model\Ticket();

        $ticketRepository = $this->getMockBuilder(\Peaksourcing\Ehpapm\Domain\Repository\TicketRepository::class)
            ->setMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $ticketRepository->expects(self::once())->method('remove')->with($ticket);
        $this->inject($this->subject, 'ticketRepository', $ticketRepository);

        $this->subject->deleteAction($ticket);
    }
}
