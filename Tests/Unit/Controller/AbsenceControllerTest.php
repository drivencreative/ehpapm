<?php
namespace Peaksourcing\Ehpapm\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 */
class AbsenceControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Peaksourcing\Ehpapm\Controller\AbsenceController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Peaksourcing\Ehpapm\Controller\AbsenceController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllAbsencesFromRepositoryAndAssignsThemToView()
    {

        $allAbsences = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $absenceRepository = $this->getMockBuilder(\Peaksourcing\Ehpapm\Domain\Repository\AbsenceRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $absenceRepository->expects(self::once())->method('findAll')->will(self::returnValue($allAbsences));
        $this->inject($this->subject, 'absenceRepository', $absenceRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('absences', $allAbsences);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenAbsenceToView()
    {
        $absence = new \Peaksourcing\Ehpapm\Domain\Model\Absence();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('absence', $absence);

        $this->subject->showAction($absence);
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenAbsenceToAbsenceRepository()
    {
        $absence = new \Peaksourcing\Ehpapm\Domain\Model\Absence();

        $absenceRepository = $this->getMockBuilder(\Peaksourcing\Ehpapm\Domain\Repository\AbsenceRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $absenceRepository->expects(self::once())->method('add')->with($absence);
        $this->inject($this->subject, 'absenceRepository', $absenceRepository);

        $this->subject->createAction($absence);
    }

    /**
     * @test
     */
    public function editActionAssignsTheGivenAbsenceToView()
    {
        $absence = new \Peaksourcing\Ehpapm\Domain\Model\Absence();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('absence', $absence);

        $this->subject->editAction($absence);
    }

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenAbsenceInAbsenceRepository()
    {
        $absence = new \Peaksourcing\Ehpapm\Domain\Model\Absence();

        $absenceRepository = $this->getMockBuilder(\Peaksourcing\Ehpapm\Domain\Repository\AbsenceRepository::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $absenceRepository->expects(self::once())->method('update')->with($absence);
        $this->inject($this->subject, 'absenceRepository', $absenceRepository);

        $this->subject->updateAction($absence);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenAbsenceFromAbsenceRepository()
    {
        $absence = new \Peaksourcing\Ehpapm\Domain\Model\Absence();

        $absenceRepository = $this->getMockBuilder(\Peaksourcing\Ehpapm\Domain\Repository\AbsenceRepository::class)
            ->setMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $absenceRepository->expects(self::once())->method('remove')->with($absence);
        $this->inject($this->subject, 'absenceRepository', $absenceRepository);

        $this->subject->deleteAction($absence);
    }
}
