<?php
namespace Peaksourcing\Ehpapm\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 */
class TaskControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Peaksourcing\Ehpapm\Controller\TaskController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Peaksourcing\Ehpapm\Controller\TaskController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllTasksFromRepositoryAndAssignsThemToView()
    {

        $allTasks = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $taskRepository = $this->getMockBuilder(\Peaksourcing\Ehpapm\Domain\Repository\TaskRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $taskRepository->expects(self::once())->method('findAll')->will(self::returnValue($allTasks));
        $this->inject($this->subject, 'taskRepository', $taskRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('tasks', $allTasks);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenTaskToView()
    {
        $task = new \Peaksourcing\Ehpapm\Domain\Model\Task();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('task', $task);

        $this->subject->showAction($task);
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenTaskToTaskRepository()
    {
        $task = new \Peaksourcing\Ehpapm\Domain\Model\Task();

        $taskRepository = $this->getMockBuilder(\Peaksourcing\Ehpapm\Domain\Repository\TaskRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $taskRepository->expects(self::once())->method('add')->with($task);
        $this->inject($this->subject, 'taskRepository', $taskRepository);

        $this->subject->createAction($task);
    }

    /**
     * @test
     */
    public function editActionAssignsTheGivenTaskToView()
    {
        $task = new \Peaksourcing\Ehpapm\Domain\Model\Task();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('task', $task);

        $this->subject->editAction($task);
    }

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenTaskInTaskRepository()
    {
        $task = new \Peaksourcing\Ehpapm\Domain\Model\Task();

        $taskRepository = $this->getMockBuilder(\Peaksourcing\Ehpapm\Domain\Repository\TaskRepository::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $taskRepository->expects(self::once())->method('update')->with($task);
        $this->inject($this->subject, 'taskRepository', $taskRepository);

        $this->subject->updateAction($task);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenTaskFromTaskRepository()
    {
        $task = new \Peaksourcing\Ehpapm\Domain\Model\Task();

        $taskRepository = $this->getMockBuilder(\Peaksourcing\Ehpapm\Domain\Repository\TaskRepository::class)
            ->setMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $taskRepository->expects(self::once())->method('remove')->with($task);
        $this->inject($this->subject, 'taskRepository', $taskRepository);

        $this->subject->deleteAction($task);
    }
}
